Critical Trick
==============

Este es un desarrollo que implementa una versión web del Truco Argentino.
El juego consiste en emplear las tres cartas que se poseen de la mejor manera posible para ganar al menos dos de las tres partes que componen cada ronda.
El objetivo es ser el primero en llegar a los quince o treinta puntos, meta que se establece antes de iniciar el partido.


# Modo de uso Localmente en linux:
## Servidor:
* Debe contener el contenido de este repositorio en si.
* Debe ingresar a la carpeta Critical/Implementacion/truco
* Dentro de esta carpeta ejecutar el comando $python manage.py runserver 
* Ahora el servidor deberia estar corriendo.

## Usuario:
* En algun browser por ejemplo firefox o chrome escribir la url:localhost:8000/critical_trick
* Esto lo redirigirá a la página de login.
* Ahora debe interactuar con el programa.


