 # -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from critical_trick.models import Partida


class RegisterForm(forms.ModelForm):
    """
    Una forma que crea un usuario, sin privilegios,
    desde el nombre de usuario dado, email y contraseña.
    """
    error_messages = {
        'duplicate_username': "Un usuario con ese nombre ya existe.",
        'password_mismatch': "Los dos campos de contraseña no coinciden.",
    }

    username = forms.RegexField(
        label=("Usuario:"), max_length=30, regex=r"^[\w.@+-]+$",
        help_text=("Necesario. 30 caracteres o menos. Letras, digitos y "
                   "@/./+/-/_ solamente."),
        error_messages={
            'invalid': ("Este valor sólo puede contener letras, números y "
                        "@/./+/-/_ caracteres.")},
        widget=forms.TextInput(
            attrs={
            }
        )
    )

    password1 = forms.CharField(
        label=("Contraseña:"),
        widget=forms.PasswordInput(
            attrs={
            }
        )
    )

    password2 = forms.CharField(
        label=("Confirmar Contraseña"),
        widget=forms.PasswordInput(
            attrs={
                'type': 'password',
            }
        ),
        help_text=("Introduzca la misma contraseña que el anterior,"
                   "para verificación.")
    )

    email = forms.CharField(
        label=("Email"),
        widget=forms.TextInput(
            attrs={
                'type': 'email',
            }
        )
    )

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    # Esto es sólo para que w3c valide como html5 el formulario
    # en la parte de required
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        for _, field in self.fields.items():
            if field.widget.is_required:
                field.widget.attrs['required'] = 'required'

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages[self.error_messages['duplicate_username']],
            code='duplicate_username',
        )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages[self.error_messages['password_mismatch']],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class LoginForm(forms.ModelForm):
    """
    Forma para el ingreso del usuario al sistema
    """

    username = forms.RegexField(
        max_length=30, regex=r"^[\w.@+-]+$",
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Usuario',
            }
        )
    )

    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Contraseña',
            }
        )
    )

    class Meta:
        model = User
        fields = ('username', 'password')

    # Esto es sólo para que w3c valide como html5 el formulario
    # en la parte de required
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        for _, field in self.fields.items():
            if field.widget.is_required:
                field.widget.attrs['required'] = 'required'


class PartidaForm(forms.ModelForm):

    class Meta:
        model = Partida
        fields = ('nombre_de_partida', 'maximo_de_jugadores', 'puntos_a_jugar')
