"""
Este archivo contendra las constantes de la aplicacion
"""

from os.path import join


###############################################################################
def obtenerSrcImagen(palo, numero):
    response = join('/static/critical_trick/cartas/',
                    str(palo), str(numero) + '.jpg')
    return response
###############################################################################
"""
Choices disponibles para la Clase Partida
"""
MAX_JUGADORES = [(2, '2'), (4, '4'), (6, '6')]
NUM_JUGADORES = [(i, str(i)) for i in range(1, 7)]
PUNTOS_JUEGO = [(15, '15'), (30, '30')]
###############################################################################
"""
Choices disponibles para la Clase Jugador
"""
EQUIPOS = [(0, '0'), (1, '1'), (2, '2')]
SILLAS = [(i, str(i)) for i in range(0, 7)]
JUG_PUNTOS_ENVIDO = [(i, str(i)) for i in range(0, 8)] + \
                    [(i, str(i)) for i in range(20, 34)]
EQUIPO_1 = 1
EQUIPO_2 = 2
###############################################################################
"""
Choices disponibles para la Clase Ronda
"""
NUMERO_DE_RONDA = [(i, str(i)) for i in range(1, 60)]
RONDA_PUNTOS_ENVIDO = [(i, str(i)) for i in range(0, 31)]
NUMERO_MANOS = [(i, str(i)) for i in range(0, 4)]
MAX_ENVIDOS = [(i, str(i)) for i in range(0, 3)]
MAX_QUIEROS = [(i, str(i)) for i in range(0, 4)]
###############################################################################
"""
Choices disponibles para la Clase Mano
"""
NUMERO_DE_MANO = [(i, str(i)) for i in range(0, 4)]
###############################################################################
"""
Choices disponibles para la Clase Carta
"""
PALOS_DE_CARTAS = (
    ('ORO',	   'oro'),
    ('COPA',   'copa'),
    ('BASTO',  'basto'),
    ('ESPADA', 'espada')
)

NUMERO = [(i, str(i)) for i in range(1, 8)] + \
         [(i, str(i)) for i in range(10, 13)]
NIVEL = [(i, str(i)) for i in range(1, 15)]
PUNTAJE_ENVIDO = [(i, str(i)) for i in range(0, 8)]
NUMERO_DE_MANO = [(i, str(i)) for i in range(0, 4)]
NUMERO_DE_RONDA = [(i, str(i)) for i in range(1, 60)]
###############################################################################
MAZO_NIVEL = {
    (1, 'ESPADA'):  1,
    (1, 'BASTO'):   2,
    (7, 'ESPADA'):  3,
    (7, 'ORO'):     4,
    (3, 'BASTO'):   5,
    (3, 'ESPADA'):  5,
    (3, 'ORO'):     5,
    (3, 'COPA'):    5,
    (2, 'BASTO'):   6,
    (2, 'ESPADA'):  6,
    (2, 'ORO'):     6,
    (2, 'COPA'):    6,
    (1, 'COPA'):    7,
    (1, 'ORO'):     7,
    (12, 'BASTO'):  8,
    (12, 'ESPADA'): 8,
    (12, 'ORO'):    8,
    (12, 'COPA'):   8,
    (11, 'BASTO'):  9,
    (11, 'ESPADA'): 9,
    (11, 'ORO'):    9,
    (11, 'COPA'):   9,
    (10, 'BASTO'):  10,
    (10, 'ESPADA'): 10,
    (10, 'ORO'):    10,
    (10, 'COPA'):   10,
    (7, 'COPA'):    11,
    (7, 'BASTO'):   11,
    (6, 'BASTO'):   12,
    (6, 'ESPADA'):  12,
    (6, 'ORO'):     12,
    (6, 'COPA'):    12,
    (5, 'BASTO'):   13,
    (5, 'ESPADA'):  13,
    (5, 'ORO'):     13,
    (5, 'COPA'):    13,
    (4, 'BASTO'):   14,
    (4, 'ESPADA'):  14,
    (4, 'ORO'):     14,
    (4, 'COPA'):    14,
}

###############################################################################
MAZO_ENVIDO = {
    (1, 'BASTO'):   1,
    (1, 'ESPADA'):  1,
    (1, 'ORO'):     1,
    (1, 'COPA'):    1,
    (2, 'BASTO'):   2,
    (2, 'ESPADA'):  2,
    (2, 'ORO'):     2,
    (2, 'COPA'):    2,
    (3, 'BASTO'):   3,
    (3, 'ESPADA'):  3,
    (3, 'ORO'):     3,
    (3, 'COPA'):    3,
    (4, 'BASTO'):   4,
    (4, 'ESPADA'):  4,
    (4, 'ORO'):     4,
    (4, 'COPA'):    4,
    (5, 'BASTO'):   5,
    (5, 'ESPADA'):  5,
    (5, 'ORO'):     5,
    (5, 'COPA'):    5,
    (6, 'BASTO'):   6,
    (6, 'ESPADA'):  6,
    (6, 'ORO'):     6,
    (6, 'COPA'):    6,
    (7, 'BASTO'):   7,
    (7, 'ESPADA'):  7,
    (7, 'ORO'):     7,
    (7, 'COPA'):    7,
    (10, 'BASTO'):  0,
    (10, 'ESPADA'): 0,
    (10, 'ORO'):    0,
    (10, 'COPA'):   0,
    (11, 'BASTO'):  0,
    (11, 'ESPADA'): 0,
    (11, 'ORO'):    0,
    (11, 'COPA'):   0,
    (12, 'BASTO'):  0,
    (12, 'ESPADA'): 0,
    (12, 'ORO'):    0,
    (12, 'COPA'):   0,
}

###############################################################################
#CONSTANTES PARA LOS TEST

form1_mismo_nombre = {
    'username': 'ironman',
    'email': 'otroIronman@marvel.com',
    'password1': 'otro_pass',
    'password2': 'otro_pass',
}
form1 = {
    'username': 'ironman',
    'email': 'ironman@marvel.com',
    'password1': 'pass',
    'password2': 'pass'
}
form2 = {
    'username': 'hulk',
    'email': 'hulk@marvel.com',
    'password1': 'green',
    'password2': 'green',
}
form_login1 = {
    'username': 'ironman',
    'password': 'pass'
}
form_login2 = {
    'username': 'hulk',
    'password': 'green',
}
form_partida = {
    'nombre_de_partida': 'test_game',
    'maximo_de_jugadores': 2,
    'puntos_a_jugar': 15,
}

form3 = {
    'username': 'capitan',
    'email': 'capitan@marvel.com',
    'password1': 'america',
    'password2': 'america'
}
form_login3 = {
    'username': 'capitan',
    'password': 'america',
}
form_partida30 = {
    'nombre_de_partida': 'truco supremo',
    'maximo_de_jugadores': 2,
    'puntos_a_jugar': 30,
}
form4 = {
    'username': 'cap',
    'email': 'cap@marvel.com',
    'password1': 'critical',
    'password2': 'critical'
}
form_login4 = {
    'username': 'cap',
    'password': 'critical'
}
form_login_empty = {
    'username': '',
    'password': '',
}
form_login_error = {
    'username': 'cap',
    'password': 'noeslapasscorrecta',
}
form_error = {
    'username': 'ironman',
    'email': 'ironman@marvel.com',
    'password1': 'pass',
    'password2': 'lalala'
}