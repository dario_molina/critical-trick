# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from random import sample
from constants import *
import json


#------------------------------------------------------------------------------
#CLASE PARTIDA
#------------------------------------------------------------------------------
class Partida(models.Model):
    nombre_de_partida = models.CharField(default="Truco Argentino",
                                         max_length=20)
    numero_jugadores = models.IntegerField(default=1,
                                           choices=NUM_JUGADORES)
    maximo_de_jugadores = models.IntegerField(default=2,
                                              choices=MAX_JUGADORES)
    puntos_a_jugar = models.IntegerField(default=15,
                                         choices=PUNTOS_JUEGO)
    puntos_equipo1 = models.IntegerField(default=0)
    puntos_equipo2 = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    numero_de_rondas = models.IntegerField(default=0, choices=NUMERO_DE_RONDA)

    def __unicode__(self):
        return unicode(self.nombre_de_partida)

    def timeThatWasCreated(self):
        return timezone.now() - self.fecha_creacion

    def siguienteRonda(self):
        ronda_actual = Ronda.objects.get(partida=self,
                                         id_ronda=self.numero_de_rondas)
        if ronda_actual.equipo_ganador == EQUIPO_1:
            self.puntos_equipo1 += ronda_actual.puntos_generales
        elif ronda_actual.equipo_ganador == EQUIPO_2:
            self.puntos_equipo2 += ronda_actual.puntos_generales
        else:
            raise Exception("Error de equipo")
        self.save()
        jugadores = Jugador.objects.filter(partida=self)
        jugadores = jugadores.order_by("silla")
        for lugar in range(0, self.maximo_de_jugadores):
            if jugadores[lugar].user.username == ronda_actual.jugador_mano:
                lugar_lado = (lugar + 1) % self.maximo_de_jugadores 
                nuevo_mano = jugadores[lugar_lado].user.username
                break
        id_ronda = self.numero_de_rondas + 1
        nueva_ronda = Ronda(partida=self, id_ronda=id_ronda,
                            jugador_mano=nuevo_mano, turno=nuevo_mano,
                            turno_canto=nuevo_mano)
        self.numero_de_rondas += 1
        nueva_ronda.save()
        self.save()

    def validarTruco(self, ronda_actual):
        response = False
        if ronda_actual.truco and not ronda_actual.re_truco:
            ronda_actual.puntos_generales += 1
            ronda_actual.quiero_truco += 1
            response = True
        elif ronda_actual.truco and ronda_actual.re_truco and \
                not ronda_actual.quiero_valecuatro:
            if ronda_actual.quiero_truco == 1:
                ronda_actual.puntos_generales += 1
                ronda_actual.quiero_truco += 1
            else:
                ronda_actual.puntos_generales += 2
                ronda_actual.quiero_truco += 2
            response = True
        elif ronda_actual.truco and ronda_actual.re_truco and \
                ronda_actual.quiero_valecuatro:
            if ronda_actual.quiero_truco == 0:
                ronda_actual.puntos_generales += 3
            elif ronda_actual.quiero_truco == 1:
                ronda_actual.puntos_generales += 2
            else:
                ronda_actual.puntos_generales += 1
            response = True
        return response

    def noValidarTruco(self, ronda_actual):
        response = False
        if ronda_actual.truco and not ronda_actual.re_truco and \
                not ronda_actual.quiero_valecuatro:
            ronda_actual.puntos_generales += 0
            response = True
        elif ronda_actual.truco and ronda_actual.re_truco and \
                not ronda_actual.quiero_valecuatro:
            ronda_actual.puntos_generales += 1
            response = True
        elif ronda_actual.truco and ronda_actual.re_truco and \
                ronda_actual.quiero_valecuatro:
            ronda_actual.puntos_generales += 2
            response = True
        else:
            raise Exception("no existe esta combinacion de truco")
        ronda_actual.save()
        return response

    def validarEnvido(self, ronda_actual):
        response = False
        if ronda_actual.envido == 1 and not ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 2
            response = True
        elif ronda_actual.envido == 2 and not ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 4
            response = True
        elif ronda_actual.envido == 0 and ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 3
            response = True
        elif ronda_actual.envido == 1 and ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 5
            response = True
        elif ronda_actual.envido == 2 and ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 7
            response = True
        elif ronda_actual.falta_envido:
            puntos_faltantes_jug1 = self.puntos_a_jugar-self.puntos_equipo1
            puntos_faltantes_jug2 = self.puntos_a_jugar-self.puntos_equipo2
            ronda_actual.puntos_envido += min(puntos_faltantes_jug1,
                                              puntos_faltantes_jug2)
            response = True
        else:
            raise Exception("muchos envidos")
        ronda_actual.save()
        return response

    def noValidarEnvido(self, ronda_actual):
        response = False
        if ronda_actual.envido == 1 and not ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 0
            response = True
        elif ronda_actual.envido == 0 and ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 0
            response = True
        elif ronda_actual.envido == 0 and not ronda_actual.real_envido and \
                ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 0
            response = True
        elif ronda_actual.envido == 2 and not ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 1
            response = True
        elif ronda_actual.envido == 1 and ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 1
            response = True
        elif ronda_actual.envido == 1 and not ronda_actual.real_envido and \
                ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 1
            response = True
        elif ronda_actual.envido == 0 and ronda_actual.real_envido and \
                ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 2
            response = True
        elif ronda_actual.envido == 2 and not ronda_actual.real_envido and \
                ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 4
            response = True
        elif ronda_actual.envido == 2 and ronda_actual.real_envido and \
                not ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 3
            response = True
        elif ronda_actual.envido == 1 and ronda_actual.real_envido and \
                ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 4
            response = True
        elif ronda_actual.envido == 2 and ronda_actual.real_envido and \
                ronda_actual.falta_envido:
            ronda_actual.puntos_envido += 6
            response = True
        else:
            raise Exception("muchos envidos")
        ronda_actual.save()
        return response

    def otorgarPuntosGanadorDeEnvido(self, ronda_actual):
        if ronda_actual.equipo_ganador_envido == EQUIPO_1:
            self.puntos_equipo1 += ronda_actual.puntos_envido
        elif ronda_actual.equipo_ganador_envido == EQUIPO_2:
            self.puntos_equipo2 += ronda_actual.puntos_envido
        else:
            raise Exception("Equipo no contemplado")
        self.save()

    def otorgarEquipo(self):
        if (self.numero_jugadores + 1) % 2 == 0:
            response = 2
        else:
            response = 1
        return response

    def lugarRepartidorDeCartas(self):
        ronda_actual = Ronda.objects.get(partida=self,
                                         id_ronda=self.numero_de_rondas)
        user_oponent = User.objects.get(username=ronda_actual.jugador_mano)
        jugador_mano = Jugador.objects.get(user=user_oponent, partida=self)
        if jugador_mano.silla == 1:
            response = self.maximo_de_jugadores
        else:
            response = jugador_mano.silla - 1
        return response



#------------------------------------------------------------------------------
#CLASE JUGADOR
#------------------------------------------------------------------------------
class Jugador(models.Model):

    equipo = models.IntegerField(max_length=1, default=0, choices=EQUIPOS)
    user = models.ForeignKey(User)
    partida = models.ForeignKey(Partida)
    silla = models.IntegerField(default=0, choices=SILLAS)
    puntos_envido = models.IntegerField(default=0, choices=JUG_PUNTOS_ENVIDO)

    def __unicode__(self):
        return unicode(self.user)

    def crearPartida(self, user_name, match_id):
        new_user = User.objects.get(username=user_name)
        new_partida = Partida.objects.get(id=match_id)
        self.user = new_user
        self.partida = new_partida
        self.equipo = EQUIPO_1
        self.silla = new_partida.numero_jugadores
        self.save()

    def sumarseAPartida(self, user_name, match_id):
        partida = Partida.objects.get(id=match_id)
        if partida.numero_jugadores >= partida.maximo_de_jugadores:
            msg_error = "Esta partida ya alcanzó su numero máximo" \
                        "de jugadores."
            raise Exception(msg_error)
        new_user = User.objects.get(username=user_name)
        self.user = new_user
        self.partida = partida
        self.equipo = self.partida.otorgarEquipo()
        self.silla = self.partida.numero_jugadores + 1
        self.save()
        self.partida.numero_jugadores += 1
        if self.partida.numero_jugadores == 2:
            mano = user_name
            id_ronda = self.partida.numero_de_rondas + 1
            nueva_ronda = Ronda(partida=self.partida, id_ronda=id_ronda,
                                jugador_mano=mano, turno=mano, turno_canto=mano)
            nueva_ronda.save()
            self.partida.numero_de_rondas += 1
        self.partida.save()

    def nombresJugadores(self):
        jugadores = Jugador.objects.filter(partida=self.partida)
        jugadores = jugadores.order_by("silla")
        sillas = [(i - self.silla) % self.partida.maximo_de_jugadores \
                  for i in range(1, self.partida.maximo_de_jugadores + 1)]
        dic = {}
        lugar = self.silla - 1
        for numero in range(1, self.partida.maximo_de_jugadores + 1):
            dic["nombre_jug" + str(numero)] = jugadores[lugar].user.username
            lugar = (lugar + 1) % self.partida.maximo_de_jugadores
        return dic

    def repartirCartas(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        if not ronda_actual.cartas_repartidas:
            jugadores = Jugador.objects.filter(partida=self.partida)
            jugadores = jugadores.order_by("silla")
            cartas_obtenidas = sample(MAZO_NIVEL,
                                      3*self.partida.maximo_de_jugadores)
            lugar = self.silla % self.partida.maximo_de_jugadores
            for k in range(len(cartas_obtenidas)):
                jugador = jugadores[lugar]
                carta_obtenida = cartas_obtenidas[k]
                nueva_carta = Carta()
                nueva_carta.jugador = jugador
                nueva_carta.ronda = ronda_actual
                nueva_carta.palo = carta_obtenida[1]
                nueva_carta.numero = carta_obtenida[0]
                nueva_carta.nivel = MAZO_NIVEL[carta_obtenida]
                nueva_carta.valor_envido = MAZO_ENVIDO[carta_obtenida]
                nueva_carta.save()
                lugar = (lugar + 1) % self.partida.maximo_de_jugadores
            ronda_actual.cartas_repartidas = True
            ronda_actual.save()
            response = True
        else:
            response = False
        return response

    def obtenerMisCartas(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        cartas = Carta.objects.filter(jugador=self, ronda=ronda_actual)
        if cartas:
            response = {"result": "cartas asignadas"}
            src1 = obtenerSrcImagen(cartas[0].palo, cartas[0].numero)
            alt1 = str(cartas[0].palo) + " " + str(cartas[0].numero)
            src2 = obtenerSrcImagen(cartas[1].palo, cartas[1].numero)
            alt2 = str(cartas[1].palo) + " " + str(cartas[1].numero)
            src3 = obtenerSrcImagen(cartas[2].palo, cartas[2].numero)
            alt3 = str(cartas[2].palo) + " " + str(cartas[2].numero)
            response["carta1"] = "<img src='%s' alt='%s' " \
                                 "height='150' width='100'>" % (src1, alt1)
            response["carta2"] = "<img src='%s' alt='%s' " \
                                 "height='150' width='100'>" % (src2, alt2)
            response["carta3"] = "<img src='%s' alt='%s' " \
                                 "height='150' width='100'>" % (src3, alt3)
            for i in range(3):
                if cartas[i].ultima:
                    response["ultima"] = "#carta" + str(i+4)
                    break
            self.calcularPuntosEnvido()
        else:
            response = {"result": "cartas sin asignar"}
        return response

    def obtenerCartasDelOponente(self, silla_op):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        oponente = Jugador.objects.get(silla=silla_op, partida=self.partida)
        init = (oponente.silla - self.silla) % self.partida.maximo_de_jugadores
        op_carta1 = "carta" + str(init * 3 + 1)
        op_carta2 = "carta" + str(init * 3 + 2)
        op_carta3 = "carta" + str(init * 3 + 3)

        cartas = Carta.objects.filter(jugador=oponente, ronda=ronda_actual)
        response = {"carta4": "", "carta5": "", "carta6": ""}
        if cartas[0].disponible_en_mesa:
            src4 = obtenerSrcImagen(cartas[0].palo, cartas[0].numero)
            alt4 = str(cartas[0].palo) + " " + str(cartas[0].numero)
            response[op_carta1] = "<img src='%s' alt='%s' " \
                                  "height='150' width='100'>" % (src4, alt4)
        if cartas[1].disponible_en_mesa:
            src5 = obtenerSrcImagen(cartas[1].palo, cartas[1].numero)
            alt5 = str(cartas[1].palo) + " " + str(cartas[1].numero)
            response[op_carta2] = "<img src='%s' alt='%s' " \
                                  "height='150' width='100'>" % (src5, alt5)
        if cartas[2].disponible_en_mesa:
            src6 = obtenerSrcImagen(cartas[2].palo, cartas[2].numero)
            alt6 = str(cartas[2].palo) + " " + str(cartas[2].numero)
            response[op_carta3] = "<img src='%s' alt='%s' " \
                                  "height='150' width='100'>" % (src6, alt6)
        for i in range(3):
            if cartas[i].ultima:
                response["ultima"] = "#carta" + str(init * 3 + i + 1)
                break
        return response

    def conocerJugadas(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        jugadas = json.loads(ronda_actual.jugadas)
        jugadas_contrario = jugadas[str(self.silla)]
        if len(jugadas_contrario) != 0:
            jugadas_contrario.reverse()
            response = jugadas_contrario.pop()
            jugadas_contrario.reverse()
            jugadas[str(self.silla)] = jugadas_contrario
            ronda_actual.jugadas = json.dumps(jugadas)
            ronda_actual.save()
        else:
            response = {"jugada": "Ninguna"}
        return response

    def elegirCarta(self, carta):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        cartas = Carta.objects.filter(jugador=self, ronda=ronda_actual)
        palo, numero = carta.split(" ")
        carta = Carta.objects.get(jugador=self, ronda=ronda_actual,
                                  palo=palo, numero=numero)
        if not carta.disponible_en_mesa:
            for c in cartas:
                c.ultima = False
                c.save()
            carta.disponible_en_mesa = True
            carta.ultima = True
            carta.save()
            num_mano_actual = ronda_actual.numero_de_manos_jugadas + 1
            manos = Mano.objects.filter(ronda=ronda_actual,
                                        id_mano=num_mano_actual)
            if len(manos) == 0:
                mano = Mano(ronda=ronda_actual, id_mano=num_mano_actual)
            elif len(manos) == 1:
                mano = manos[0]
            else:
                raise Exception("no puede haber mas de 1 mano")
            if self.silla == 1:
                mano.carta_jugador1 = palo + " " + numero
            if self.silla == 2:
                mano.carta_jugador2 = palo + " " + numero
            elif self.silla == 3:
                mano.carta_jugador3 = palo + " " + numero
            elif self.silla == 4:
                mano.carta_jugador4 = palo + " " + numero
            elif self.silla == 5:
                mano.carta_jugador5 = palo + " " + numero
            elif self.silla == 6:
                mano.carta_jugador6 = palo + " " + numero
            mano.save()
            ronda_actual.pasarTurno(self)
            ronda_actual.pasarTurnoCanto(self)
            ronda_actual.setearJugada(self.equipo, self.silla, "eleccion de carta")
            ronda_actual.save()
        return ronda_actual.turno

    def envido(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        response = ronda_actual.turno_canto
        if ronda_actual.envido <= 1:
            ronda_actual.envido += 1
            ronda_actual.setearJugada(self.equipo, self.silla, "envido")
            ronda_actual.pasarTurnoCanto(self)
            ronda_actual.save()
            response = ronda_actual.turno_canto
        return response

    def realEnvido(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        response = ronda_actual.turno_canto
        if not ronda_actual.real_envido:
            ronda_actual.real_envido = True
            ronda_actual.setearJugada(self.equipo, self.silla, "real envido")
            ronda_actual.pasarTurnoCanto(self)
            ronda_actual.save()
            response = ronda_actual.turno_canto
        return response

    def faltaEnvido(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        response = ronda_actual.turno_canto
        if not ronda_actual.falta_envido:
            ronda_actual.falta_envido = True
            ronda_actual.setearJugada(self.equipo, self.silla, "falta envido")
            ronda_actual.pasarTurnoCanto(self)
            ronda_actual.save()
            response = ronda_actual.turno_canto
        return response

    def calcularPuntosEnvido(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        cartas = Carta.objects.filter(jugador=self, ronda=ronda_actual)
        cartas_mismo_palo = []

        if cartas[0].palo == cartas[1].palo:
            cartas_mismo_palo.append(cartas[0])
            cartas_mismo_palo.append(cartas[1])
            if cartas[0].palo == cartas[2].palo:
                cartas_mismo_palo.append(cartas[2])
        elif cartas[0].palo == cartas[2].palo:
            cartas_mismo_palo.append(cartas[0])
            cartas_mismo_palo.append(cartas[2])
        elif cartas[1].palo == cartas[2].palo:
            cartas_mismo_palo.append(cartas[1])
            cartas_mismo_palo.append(cartas[2])
        else:
            pass

        if len(cartas_mismo_palo) == 0:
            puntos = max(cartas[0].valor_envido,
                         cartas[1].valor_envido,
                         cartas[2].valor_envido,)
        elif len(cartas_mismo_palo) == 2:
            c0 = cartas_mismo_palo[0]
            c1 = cartas_mismo_palo[1]
            puntos = c0.valor_envido + c1.valor_envido + 20
        elif len(cartas_mismo_palo) == 3:
            c0 = cartas_mismo_palo[0]
            c1 = cartas_mismo_palo[1]
            c2 = cartas_mismo_palo[2]
            puntos = max(c0.valor_envido+c1.valor_envido,
                         c0.valor_envido+c2.valor_envido,
                         c1.valor_envido+c2.valor_envido,) + 20
        else:
            raise Exception("caso no contemplado")
        self.puntos_envido = puntos
        self.save()
        return puntos

    def cantarPuntosEnvido(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        ronda_actual.setearJugada(self.equipo, self.silla, "Puntos",
                                  extra=self.puntos_envido)
        ronda_actual.pasarTurnoCanto(self)
        self.partida.otorgarPuntosGanadorDeEnvido(ronda_actual)
        ronda_actual.save()
        return ronda_actual.turno_canto

    def sonBuenas(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        ronda_actual.setearJugada(self.equipo, self.silla, "Son Buenas")
        ronda_actual.pasarTurnoCanto(self)
        ronda_actual.son_buenas += 1
        if ronda_actual.son_buenas == self.partida.numero_jugadores - 1:
            ronda_actual.setearJugada(self.equipo, self.silla, "Activar Truco",
                                      general=True)
        ronda_actual.save()
        response = ronda_actual.turno_canto
        return response

    def truco(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        response = ronda_actual.turno_canto
        if not ronda_actual.truco:
            ronda_actual.truco = True
            ronda_actual.setearJugada(self.equipo, self.silla, "truco")
            ronda_actual.pasarTurnoCanto(self)
            ronda_actual.save()
            response = ronda_actual.turno_canto
        return response

    def reTruco(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        response = ronda_actual.turno_canto
        if ronda_actual.truco and not ronda_actual.re_truco:
            ronda_actual.re_truco = True
            ronda_actual.setearJugada(self.equipo, self.silla, "re truco")
            ronda_actual.pasarTurnoCanto(self)
            ronda_actual.save()
            response = ronda_actual.turno_canto
        return response

    def quieroValeCuatro(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        response = ronda_actual.turno_canto
        if ronda_actual.re_truco and not ronda_actual.quiero_valecuatro:
            ronda_actual.quiero_valecuatro = True
            ronda_actual.setearJugada(self.equipo, self.silla, "quiero vale cuatro")
            ronda_actual.pasarTurnoCanto(self)
            ronda_actual.save()
            response = ronda_actual.turno_canto
        return response

    def quiero(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        response = ronda_actual.turno_canto
        if not ronda_actual.truco:
            if self.partida.validarEnvido(ronda_actual):
                ronda_actual.setearJugada(self.equipo, self.silla, "quiero envido")
                ronda_actual.save()
                response = ronda_actual.turno_canto
        else:
            if self.partida.validarTruco(ronda_actual):
                ronda_actual.setearJugada(self.equipo, self.silla, "quiero truco")
                ronda_actual.pasarTurnoCanto(self)
                response = ronda_actual.turno_canto
        return response

    def noQuiero(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        response = ronda_actual.turno_canto
        if not ronda_actual.truco:
            if self.partida.noValidarEnvido(ronda_actual):
                ronda_actual.setearJugada(self.equipo, self.silla, "no quiero envido")
                if self.equipo == EQUIPO_1:
                    ronda_actual.equipo_ganador_envido = EQUIPO_2
                elif self.equipo == EQUIPO_2:
                    ronda_actual.equipo_ganador_envido = EQUIPO_1
                else:
                    raise Exception("equipo no contemplado")
                ronda_actual.save()
                self.partida.otorgarPuntosGanadorDeEnvido(ronda_actual)
                ronda_actual.pasarTurnoCanto(self, direccion="izquierda")
                response = ronda_actual.turno_canto
        else:
            if self.partida.noValidarTruco(ronda_actual):
                ronda_actual.setearJugada(self.equipo, self.silla, "no quiero truco")
                if self.equipo == EQUIPO_1:
                    ronda_actual.equipo_ganador = EQUIPO_2
                elif self.equipo == EQUIPO_2:
                    ronda_actual.equipo_ganador = EQUIPO_1
                else:
                    raise Exception("Equipo no considerado")
                ronda_actual.save()
                response = ronda_actual.turno_canto
        return response

    def irAlMazo(self):
        numero_de_rondas = self.partida.numero_de_rondas
        ronda_actual = Ronda.objects.get(partida=self.partida,
                                         id_ronda=numero_de_rondas)
        jugadores = Jugador.objects.filter(partida=self.partida)
        if ronda_actual.numero_de_manos_jugadas < 1:
            ronda_actual.puntos_generales += 1
        if self.equipo == EQUIPO_1:
            ronda_actual.equipo_ganador = EQUIPO_2
        elif self.equipo == EQUIPO_2:
            ronda_actual.equipo_ganador = EQUIPO_1
        else:
            raise Exception("Equipo no considerado")
        ronda_actual.save()
        ronda_actual.setearJugada(self.equipo, self.silla, "me voy al mazo")


#------------------------------------------------------------------------------
#CLASE RONDA
#------------------------------------------------------------------------------
class Ronda(models.Model):

    partida = models.ForeignKey(Partida)
    id_ronda = models.IntegerField(max_length=2, choices=NUMERO_DE_RONDA)
    equipo_ganador = models.IntegerField(max_length=1, choices=EQUIPOS, default=0)
    puntos_generales = models.IntegerField(default=1)
    puntos_envido = models.IntegerField(default=0, choices=RONDA_PUNTOS_ENVIDO)
    numero_de_manos_jugadas = models.IntegerField(default=0,
                                                  choices=NUMERO_MANOS)
    jugador_mano = models.CharField(max_length=30)
    turno = models.CharField(max_length=30)
    turno_canto = models.CharField(max_length=30)
    cartas_repartidas = models.BooleanField(default=False)
    ganador_mano1 = models.CharField(max_length=30, default="Ninguno")
    ganador_mano2 = models.CharField(max_length=30, default="Ninguno")
    ganador_mano3 = models.CharField(max_length=30, default="Ninguno")
    equipo_ganador_envido = models.IntegerField(default=0, max_length=1)
    jugadas = models.TextField(default=json.dumps({1: [], 2: [], 3:[], 4: [], 5: [], 6: []}))
    envido = models.IntegerField(default=0, max_length=1, choices=MAX_ENVIDOS)
    truco = models.BooleanField(default=False)
    re_truco = models.BooleanField(default=False)
    quiero_valecuatro = models.BooleanField(default=False)
    quiero_truco = models.IntegerField(default=0, max_length=1,
                                       choices=MAX_QUIEROS)
    real_envido = models.BooleanField(default=False)
    falta_envido = models.BooleanField(default=False)
    son_buenas = models.IntegerField(default=0, max_length=1)

    def __unicode__(self):
        return unicode(self.id_ronda)

    def ganadorRonda(self):
        if self.numero_de_manos_jugadas == 1:
            ganador = "no se puede decidir"
        elif self.numero_de_manos_jugadas == 2:
            if self.ganador_mano1 == self.ganador_mano2 and \
                    self.ganador_mano1 != "Empate":
                ganador = self.ganador_mano1
            elif self.ganador_mano1 == "Empate" and \
                    self.ganador_mano2 != "Empate":
                ganador = self.ganador_mano2
            elif self.ganador_mano1 != "Empate" and \
                    self.ganador_mano2 == "Empate":
                ganador = self.ganador_mano1
            else:
                ganador = "no se puede decidir"
        elif self.numero_de_manos_jugadas == 3:
            if self.ganador_mano3 != "Empate":
                ganador = self.ganador_mano3
            elif self.ganador_mano3 == "Empate":
                if self.ganador_mano1 != "Empate":
                    ganador = self.ganador_mano1
                elif self.ganador_mano1 == "Empate":
                    user = User.objects.get(username=self.jugador_mano)
                    jugador_mano = Jugador.objects.get(user=user,
                                                       partida=self.partida)
                    ganador = jugador_mano.equipo

        if ganador != "no se puede decidir":
            self.equipo_ganador = ganador
            self.save()

        return ganador

    def ganadorDeEnvido(self):
        jugadores = Jugador.objects.filter(partida=self.partida)
        jugadores = jugadores.order_by("silla")
        lista_de_puntos = []
        for jugador in jugadores:
            lista_de_puntos.append((jugador.puntos_envido, jugador))
        jugador_ganador = max(lista_de_puntos)[1]
        self.turno_canto = jugador_ganador.user.username
        self.equipo_ganador_envido = jugador_ganador.equipo
        self.save()
        return jugador_ganador.user.username

    def setearJugada(self, equipo, silla, jugada, extra="", general=False):
        sillas_jugadores = range(1, self.partida.maximo_de_jugadores + 1)
        if not general:
            sillas_jugadores.remove(silla)
        nueva_jugada = {'equipo': equipo, 'silla': silla, 'jugada':jugada,
                        'extra': extra}
        jugadas = json.loads(self.jugadas)
        for i in sillas_jugadores:
            jugadas[str(i)].append(nueva_jugada)
        self.jugadas = json.dumps(jugadas)
        self.save()

    def definirGanadorMano(self, equipo_ganador):
        sillas_jugadores = range(1, self.partida.maximo_de_jugadores + 1)
        nueva_jugada = {'jugada': "ganador de mano", 'ganador': equipo_ganador}
        jugadas = json.loads(self.jugadas)
        for i in sillas_jugadores:
            jugadas[str(i)].append(nueva_jugada)
        self.jugadas = json.dumps(jugadas)
        self.save()

    def pasarTurno(self, jugador):
        jugadores = Jugador.objects.filter(partida=self.partida)
        jugadores = jugadores.order_by("silla")
        lugar = jugador.silla % self.partida.maximo_de_jugadores
        self.turno = jugadores[lugar].user.username
        self.save()

    def pasarTurnoCanto(self, jugador, direccion="derecha"):
        jugadores = Jugador.objects.filter(partida=self.partida)
        jugadores = jugadores.order_by("silla")
        if direccion == "derecha" :
            lugar = jugador.silla % self.partida.maximo_de_jugadores
        elif direccion == "izquierda":
            if jugador.silla == 1:
                lugar = self.partida.maximo_de_jugadores - 1
            else:
                lugar = jugador.silla - 2
        self.turno_canto = jugadores[lugar].user.username
        self.save()
        return self.turno_canto


#------------------------------------------------------------------------------
#CLASE MANO
#------------------------------------------------------------------------------
class Mano(models.Model):

    ronda = models.ForeignKey(Ronda)
    id_mano = models.IntegerField(max_length=1, choices=NUMERO_DE_MANO,
                                  default=0)
    carta_jugador1 = models.CharField(max_length=9, default="ninguna")
    carta_jugador2 = models.CharField(max_length=9, default="ninguna")
    carta_jugador3 = models.CharField(max_length=9, default="ninguna")
    carta_jugador4 = models.CharField(max_length=9, default="ninguna")
    carta_jugador5 = models.CharField(max_length=9, default="ninguna")
    carta_jugador6 = models.CharField(max_length=9, default="ninguna")
    finalizada = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.id_mano)

    def ganadorDeMano(self):
        partida = Partida.objects.get(ronda=self.ronda)
        flag = False
        if partida.maximo_de_jugadores == 2:
            if self.carta_jugador1 != "ninguna" and \
                    self.carta_jugador2 != "ninguna":
                palo1, numero1 = self.carta_jugador1.split(" ")
                palo2, numero2 = self.carta_jugador2.split(" ")
                carta1 = Carta.objects.get(ronda=self.ronda, palo=palo1,
                                           numero=numero1)
                carta2 = Carta.objects.get(ronda=self.ronda, palo=palo2,
                                           numero=numero2)
                cartas_de_jugadores = [carta1, carta2]
                flag = True
        elif partida.maximo_de_jugadores == 4:
            if self.carta_jugador1 != "ninguna" and \
                    self.carta_jugador2 != "ninguna" and \
                    self.carta_jugador3 != "ninguna" and \
                    self.carta_jugador4 != "ninguna":
                palo1, numero1 = self.carta_jugador1.split(" ")
                palo2, numero2 = self.carta_jugador2.split(" ")
                palo3, numero3 = self.carta_jugador3.split(" ")
                palo4, numero4 = self.carta_jugador4.split(" ")
                carta1 = Carta.objects.get(ronda=self.ronda, palo=palo1,
                                           numero=numero1)
                carta2 = Carta.objects.get(ronda=self.ronda, palo=palo2,
                                           numero=numero2)
                carta3 = Carta.objects.get(ronda=self.ronda, palo=palo3,
                                           numero=numero3)
                carta4 = Carta.objects.get(ronda=self.ronda, palo=palo4,
                                           numero=numero4)
                cartas_de_jugadores = [carta1, carta2, carta3, carta4]
                flag = True
        elif partida.maximo_de_jugadores == 6:
            if self.carta_jugador1 != "ninguna" and \
                    self.carta_jugador2 != "ninguna" and \
                    self.carta_jugador3 != "ninguna" and \
                    self.carta_jugador4 != "ninguna" and \
                    self.carta_jugador5 != "ninguna" and \
                    self.carta_jugador6 != "ninguna":
                palo1, numero1 = self.carta_jugador1.split(" ")
                palo2, numero2 = self.carta_jugador2.split(" ")
                palo3, numero3 = self.carta_jugador3.split(" ")
                palo4, numero4 = self.carta_jugador4.split(" ")
                palo5, numero5 = self.carta_jugador5.split(" ")
                palo6, numero6 = self.carta_jugador6.split(" ")
                carta1 = Carta.objects.get(ronda=self.ronda, palo=palo1,
                                           numero=numero1)
                carta2 = Carta.objects.get(ronda=self.ronda, palo=palo2,
                                           numero=numero2)
                carta3 = Carta.objects.get(ronda=self.ronda, palo=palo3,
                                           numero=numero3)
                carta4 = Carta.objects.get(ronda=self.ronda, palo=palo4,
                                           numero=numero4)
                carta5 = Carta.objects.get(ronda=self.ronda, palo=palo5,
                                           numero=numero5)
                carta6 = Carta.objects.get(ronda=self.ronda, palo=palo6,
                                           numero=numero6)
                cartas_de_jugadores = [carta1, carta2, carta3,
                                       carta4, carta5, carta6]
                flag = True
        if flag:
            cartas_equipo1 = []
            cartas_equipo2 = []
            for carta in cartas_de_jugadores:
                if carta.jugador.equipo == EQUIPO_1:
                    cartas_equipo1.append((carta.nivel, carta))
                elif carta.jugador.equipo == EQUIPO_2:
                    cartas_equipo2.append((carta.nivel, carta))
            carta_equipo1 = min(cartas_equipo1)[1]
            carta_equipo2 = min(cartas_equipo2)[1]
            if carta_equipo1.nivel < carta_equipo2.nivel:
                ganador = carta_equipo1.jugador.user.username
                equipo_ganador = carta_equipo1.jugador.equipo
            elif carta_equipo1.nivel > carta_equipo2.nivel:
                ganador = carta_equipo2.jugador.user.username
                equipo_ganador = carta_equipo2.jugador.equipo
            else:
                equipo_ganador = "Empate"

            if equipo_ganador != "Empate":
                self.ronda.turno = ganador
                self.ronda.turno_canto = ganador
            else:
                self.ronda.turno = self.ronda.jugador_mano
                self.ronda.turno_canto = self.ronda.jugador_mano

            if self.id_mano == 1:
                self.ronda.ganador_mano1 = equipo_ganador
            elif self.id_mano == 2:
                self.ronda.ganador_mano2 = equipo_ganador
            elif self.id_mano == 3:
                self.ronda.ganador_mano3 = equipo_ganador

            self.ronda.numero_de_manos_jugadas += 1
            self.finalizada = True
            self.save()
            self.ronda.save()
            self.ronda.definirGanadorMano(equipo_ganador)

#------------------------------------------------------------------------------
#CLASE CARTA
#------------------------------------------------------------------------------
class Carta(models.Model):

    jugador = models.ForeignKey(Jugador)
    ultima = models.BooleanField(default=False)
    ronda = models.ForeignKey(Ronda)
    palo = models.CharField(max_length=6, choices=PALOS_DE_CARTAS)
    numero = models.IntegerField(max_length=2, choices=NUMERO)
    nivel = models.IntegerField(max_length=2, choices=NIVEL)
    valor_envido = models.IntegerField(max_length=1, choices=PUNTAJE_ENVIDO)
    disponible_en_mesa = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.palo) + unicode(self.numero)
