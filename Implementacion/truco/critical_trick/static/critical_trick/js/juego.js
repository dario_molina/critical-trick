color_j1 = '#5882FA'
color_j2 = '#64FE2E'
color_s = '#FE2E2E'

function inicializarClickBotones() {
    $("#truco").click(truco);
    $("#re_truco").click(reTruco);
    $("#quiero_valecuatro").click(quieroValeCuatro);
    $("#envido").click(envido);
    $("#real_envido").click(realEnvido);
    $("#falta_envido").click(faltaEnvido);  
    $("#ir_al_mazo").click(irAlMazo);
}

$(document).ready( function() {
    if (PUNTOS_EQUIPO1 >= 15) {
        showInfo("El equipo 1 a ganado la Partida!!!", 4000, color_s);
        setTimeout( function() {
            $(location).attr('href', URL_MENU);
        }, 4000);
    } else if (PUNTOS_EQUIPO2 >= 15) {
        showInfo("El equipo 2 a ganado la Partida!!!", 4000, color_s);
        setTimeout( function() {
            $(location).attr('href', URL_MENU);
        }, 4000);
    } else {
        if (!CARTAS_REPARTIDAS) {
            $.get(URL_LUGAR_REPARTIDOR, function(response) {
                if (response == MI_SILLA) {
                    showInfo("Debes repartir las cartas!", 4000, color_s)
                    document.getElementById('repartir').style.display = 'block';
                } else {
                    showInfo("Espera a que repartan las cartas!", 4000, color_s);
                }
            });
        }
        inicializarClickBotones()
        if (NUMERO_JUGADORES == 4) {
            setNombres();
        }
        var interval = setInterval( function() {
            actualizarMisCartas(interval);
        }, 2000);
    }
});

function setNombres() {
    $.get(URL_NOMBRES_JUGADORES, function(response) {
        document.getElementById("nombre_jug1").innerHTML = response.nombre_jug1
        document.getElementById("nombre_jug2").innerHTML = response.nombre_jug2
        document.getElementById("nombre_jug3").innerHTML = response.nombre_jug3
        document.getElementById("nombre_jug4").innerHTML = response.nombre_jug4
    });
}

function repartirCartas() {
    $.get(URL_REPARTIR_CARTAS, function(response) {
            document.getElementById('repartir').style.display = 'none';
            if (response != "True") {
                showInfo("Las Cartas ya fueron repartidas", 2000, color_s);
            }
        }
    );
}

function actualizarMisCartas(interval) {
    $.get(URL_CARTAS_DEL_JUGADOR, function(response) {
        if (response.result == "cartas asignadas") {
            clearInterval(interval);
            back_side_card = "<img src="+URL_CARTA_BACK+" alt='back' height='150' width='100'>"
            $("#carta1").html(response.carta1);
            setTimeout( function() {$("#carta4").html(back_side_card);}, 50);
            setTimeout( function() {$("#carta2").html(response.carta2);}, 100);
            setTimeout( function() {$("#carta5").html(back_side_card);}, 150);
            setTimeout( function() {$("#carta3").html(response.carta3);}, 200);
            setTimeout( function() {$("#carta6").html(back_side_card);}, 250);
            if (NUMERO_JUGADORES >= 4) {
                $("#carta7").html(back_side_card);
                $("#carta8").html(back_side_card);
                $("#carta9").html(back_side_card);
                $("#carta10").html(back_side_card);
                $("#carta11").html(back_side_card);
                $("#carta12").html(back_side_card);
            }
            if (TRUCO && TURNO == USERNAME) {
                invalidarCartas()
            } else {
                validarCartas();
            }
            EmpezarInteraccion();
        }
    });
}

function EmpezarInteraccion() {
    interval2 = setInterval( function() {
        respuestaDelOponente(interval2);
    }, 1000);
}

function validarCartas() {
    $("#carta1").click(function() {obtenerTurno(enviarCarta, "#carta1")});
    $("#carta2").click(function() {obtenerTurno(enviarCarta, "#carta2")});
    $("#carta3").click(function() {obtenerTurno(enviarCarta, "#carta3")});
}

function invalidarCartas(){
    $("#carta1").unbind( "click" );
    $("#carta2").unbind( "click" );
    $("#carta3").unbind( "click" );
}

function respuestaDelOponente(interval2){
    $.get(URL_JUGADA_DEL_OPONENTE, function(response) {
        switch (response.jugada) {
            case "eleccion de carta":
                cartaDelOponente(response.silla);
                break;
            case "ganador de mano":
                ganadorDeMano(response.ganador);
                break;
            case "truco":
                showInfo("Truco!!", 2000, color_j2);
                botonesInvisibles(['truco', 'envido', 'real_envido',
                                   'falta_envido']);
                if (response.equipo != MI_EQUIPO) {
                    botonesVisibles(['re_truco'])
                }
                var activar_funciones = function() {
                    invalidarCartas();
                    botonesVisibles(['quiero', 'no_quiero'])
                    $("#quiero").unbind( "click" );
                    $("#quiero").on("click", quieroTruco);
                    $("#no_quiero").unbind( "click" );
                    $("#no_quiero").on("click", noQuieroTruco);
                }
                obtenerTurnoCanto(activar_funciones, arg=null, show_msg=false);
                break;
            case "re truco":
                showInfo("Quiero Re Truco!!", 2000, color_j2);
                botonesInvisibles(['re_truco'])
                if (response.equipo != MI_EQUIPO) {
                    botonesVisibles(['quiero_valecuatro']);
                }
                var activar_funciones = function() {
                    invalidarCartas();
                    botonesVisibles(['quiero', 'no_quiero']);
                    $("#quiero").unbind( "click" );
                    $("#quiero").on("click", quieroTruco);
                    $("#no_quiero").unbind( "click" );
                    $("#no_quiero").on("click", noQuieroTruco);
                }
                obtenerTurnoCanto(activar_funciones, arg=null, show_msg=false);
                break;
            case "quiero vale cuatro":
                showInfo("Quiero Vale Cuatro!!", 2000, color_j2);
                invalidarCartas();
                botonesInvisibles(['quiero_valecuatro']);
                var activar_funciones = function() {
                    botonesVisibles(['quiero', 'no_quiero']);
                    $("#quiero").unbind( "click" );
                    $("#quiero").on("click", quieroTruco);
                    $("#no_quiero").unbind( "click" );
                    $("#no_quiero").on("click", noQuieroTruco);
                }
                obtenerTurnoCanto(activar_funciones, arg=null, show_msg=false);
                break;
            case "envido":
                showInfo("Envido!!", 2000, color_j2);
                var activar_funciones = function() {
                    invalidarCartas();
                    botonesInvisibles(['truco']);
                    botonesVisibles(['envido', 'real_envido', 'falta_envido',
                                     'quiero', 'no_quiero']);
                    $("#truco").unbind( "click" );
                    $("#quiero").unbind( "click" );
                    $("#quiero").on("click", quieroEnvido);
                    $("#no_quiero").unbind( "click" );
                    $("#no_quiero").on("click", noQuieroEnvido);
                    $("#envido").on("click", envido);
                }
                obtenerTurnoCanto(activar_funciones, arg=null, show_msg=false);
                break;
            case "real envido":
                showInfo("Real Envido!!", 2000, color_j2);
                var activar_funciones = function() {
                    invalidarCartas();
                    botonesInvisibles(['truco', 'envido', 'real_envido']);
                    botonesVisibles(['falta_envido', 'quiero', 'no_quiero']);
                    $("#truco").unbind( "click" );
                    $("#quiero").unbind( "click" );
                    $("#quiero").on("click", quieroEnvido);
                    $("#no_quiero").unbind( "click" );
                    $("#no_quiero").on("click", noQuieroEnvido);
                    $("#falta_envido").on("click", faltaEnvido);
                }
                obtenerTurnoCanto(activar_funciones, arg=null, show_msg=false);
                break;
            case "falta envido":
                showInfo("Falta Envido!!", 2000, color_j2);
                var activar_funciones = function() {
                    invalidarCartas();
                    botonesInvisibles(['truco', 'envido', 'real_envido',
                                       'falta_envido']);
                    botonesVisibles(['quiero', 'no_quiero']);
                    $("#truco").unbind( "click" );
                    $("#quiero").unbind( "click" );
                    $("#quiero").on("click", quieroEnvido);
                    $("#no_quiero").unbind( "click" );
                    $("#no_quiero").on("click", noQuieroEnvido);
                }
                obtenerTurnoCanto(activar_funciones, arg=null, show_msg=false);
                break;
            case "quiero truco":
                showInfo("Quiero!", 2000, color_j2);
                validarCartas();
                break;
            case "no quiero truco":
                clearInterval(interval2);
                invalidarCartas();
                showInfo("No Quiero!", 2000, color_j2);
                botonesInvisibles(['truco', 're_truco', 'quiero_valecuatro',
                                   'envido', 'real_envido', 'falta_envido',
                                   'quiero', 'no_quiero', 'ir_al_mazo']);
                setTimeout( function() {
                    $(location).attr('href', URL_JUEGO);
                }, 2000);
                break;
            case "quiero envido":
                showInfo("Quiero!", 2000, color_j2);
                botonesInvisibles(['truco', 'envido', 'real_envido',
                                   'falta_envido']);
                ganadorDeEnvido();
                break;
            case "no quiero envido":
                showInfo("No Quiero!", 2000, color_j2)
                validarCartas();
                botonesInvisibles(['envido', 'real_envido', 'falta_envido']);
                botonesVisibles(['truco']);
                $("#truco").click(truco);
                break;
            case "Puntos":
                showInfo("Tengo"+response.extra+"!", 2000, color_j2);
                break;
            case "Son Buenas":
                showInfo("Son Buenas!", 2000, color_j2);
                break;
            case "Activar Truco":
                botonesVisibles(['truco']);
                $("#truco").click(truco);
                validarCartas();
                break;
            case "me voy al mazo":
                clearInterval(interval2);
                invalidarCartas();
                showInfo("Me voy al mazo!", 2000, color_j2);
                botonesInvisibles(['truco', 're_truco', 'quiero_valecuatro',
                                   'envido', 'real_envido', 'falta_envido',
                                   'quiero', 'no_quiero', 'ir_al_mazo',
                                   'puntos_envido', 'son_buenas']);
                setTimeout( function() {
                    $(location).attr('href', URL_JUEGO);
                }, 2000);
                break;
            default:
                break;
        }
    });
}

function obtenerTurno(fun, arg){
    $.get(URL_OBTENER_TURNO, function(response) {
        if (response == USERNAME) {
            fun(arg)
        } else {
            showInfo("Es turno de " + response, 2000, color_s);
        }
    });
}

function obtenerTurnoCanto(fun, arg, show_msg){
    if (typeof(show_msg)==='undefined') show_msg = true;
    $.get(URL_OBTENER_TURNO_CANTO, function(response) {
        if (response == USERNAME) {
            fun(arg)
        } else {
            if (show_msg) showInfo("Es turno de " + response, 2000, color_s);
        }
    });
}

function enviarCarta(carta_jugador) {
    var info_carta = $(carta_jugador.concat(" img")).attr('alt');
    $.get(URL_SELECCIONAR_CARTA, {carta: info_carta},
        function(response) {
            if (response == USERNAME) {
                showInfo("Debes elegir otra carta", 2000, color_s);
            } else {
                showInfo("esperar carta de " + response, 2000, color_s);
                $(carta_jugador.concat(" img")).css({"opacity":"0.7"});
                $(carta_jugador.concat(" img")).css({"top": "10px"});
                $(carta_jugador.concat(" img")).css({"border": "3px solid "+color_j1});
                $(carta_jugador).unbind( "click" );
                botonesInvisibles(['envido', 'real_envido', 'falta_envido'])
                $("#envido").unbind( "click" );
            }
        }
    );
}

function cartaDelOponente(silla) {
    $.get(URL_CARTAS_DEL_OPONENTE, {silla: silla}, function(response) {
        if (response.carta4 != "") $("#carta4").html(response.carta4);
        if (response.carta5 != "") $("#carta5").html(response.carta5);
        if (response.carta6 != "") $("#carta6").html(response.carta6);
        $("#carta4".concat(" img")).css({"top": "0px"});
        $("#carta5".concat(" img")).css({"top": "0px"});
        $("#carta6".concat(" img")).css({"top": "0px"});

        if (NUMERO_JUGADORES >= 4) {
            if (response.carta7 != "") $("#carta7").html(response.carta7);
            if (response.carta8 != "") $("#carta8").html(response.carta8);
            if (response.carta9 != "") $("#carta9").html(response.carta9);
            if (response.carta10 != "") $("#carta10").html(response.carta10);
            if (response.carta11 != "") $("#carta11").html(response.carta11);
            if (response.carta12 != "") $("#carta12").html(response.carta12);
            $("#carta7".concat(" img")).css({"top": "0px"});
            $("#carta8".concat(" img")).css({"top": "0px"});
            $("#carta9".concat(" img")).css({"top": "0px"});
            $("#carta10".concat(" img")).css({"top": "0px"});
            $("#carta11".concat(" img")).css({"top": "0px"});
            $("#carta12".concat(" img")).css({"top": "0px"});
        }
        $(response.ultima.concat(" img")).css({"top": "40px"});
        $(response.ultima.concat(" img")).css({"border": "3px solid "+color_j2});
    });
}

function ganadorDeMano(response) {
    if (response == "falta una carta" || response == "Ninguno") {
    } else if (response == "Empate") {
        showInfo("Se a declarado un empate en la mano!", 3000, color_s);
        setTimeout( function() {
            resetStyles1()
            resetStyles2()
            resetStyles3()
            resetStyles4()
        }, 3000);
        ganadorDeRonda();
    } else if (response == MI_EQUIPO) {
        showInfo("Su equipo a ganado la mano!", 3000, color_j1);
        setTimeout( function() {
            resetStyles1()
            resetStyles2()
            resetStyles3()
            resetStyles4()
        }, 3000);
        ganadorDeRonda();
    } else {
        setTimeout( function() {
            resetStyles1()
            resetStyles2()
            resetStyles3()
            resetStyles4()
        }, 3000);
        ganadorDeRonda();
        showInfo("Equipo " + response + " se a llevado la mano!", 3000, color_j2);
    }
}

function ganadorDeRonda() {
    $.get(URL_GANADOR_DE_RONDA, function(response) {
        if (response != "no se puede decidir") {
            clearInterval(interval2)
            if (response == MI_EQUIPO) {
                color_ganador = color_j1
            } else {
                color_ganador = color_j2
            }
            showInfo("Equipo" + response + " a ganado la ronda!!!", 4000, color_ganador)
            setTimeout( function() {
                if (JUGADOR_MANO == USERNAME) {
                    siguienteRonda();
                }
                setTimeout( function() {
                    $(location).attr('href', URL_JUEGO);
                }, 2000);
            }, 2000);
        }
    });
}

function siguienteRonda() {
    $.get(URL_SIGUIENTE_RONDA);
}

function envido() {
    var cantarEnvido = function() {
        $.get(URL_CANTAR_ENVIDO, function(response) {
            if (response != USERNAME) {
                invalidarCartas()
                $("#truco").unbind( "click" );
                $("#envido").on("click", envido);
                showInfo("Envido!!", 2500, color_j1)
                botonesInvisibles(['truco', 'envido', 'real_envido',
                                   'falta_envido', 'quiero', 'no_quiero']);
            } else {
                showInfo("Envido no disponible.", 2000, color_s)
            }
        });
    }
    obtenerTurnoCanto(cantarEnvido);
}

function realEnvido() {
    var cantarRealEnvido = function() {
        $.get(URL_CANTAR_REAL_ENVIDO, function(response) {
            if (response != USERNAME) {
                invalidarCartas()
                $("#truco").unbind( "click" );
                $("#real_envido").on("click", real_envido);
                showInfo("Real Envido!!", 2500, color_j1)
                botonesInvisibles(['truco', 'envido', 'real_envido',
                                   'falta_envido', 'quiero', 'no_quiero']);
            } else {
                showInfo("Real Envido no disponible.", 2000, color_s)
            }
        });
    }
    obtenerTurnoCanto(cantarRealEnvido);
}

function faltaEnvido() {
    var cantarFaltaEnvido = function() {
        $.get(URL_CANTAR_FALTA_ENVIDO, function(response) {
            if (response != USERNAME) {
                invalidarCartas()
                $("#truco").unbind( "click" );
                $("#falta_envido").on("click", falta_envido);
                showInfo("Falta Envido!!", 2500, color_j1)
                botonesInvisibles(['truco', 'envido', 'real_envido',
                                   'falta_envido', 'quiero', 'no_quiero']);
            } else {
                showInfo("Falta Envido no disponible.", 2000, color_s)
            }
        });
    }
    obtenerTurnoCanto(cantarFaltaEnvido);
}

function truco() {
    var cantarTruco = function() { 
        $.get(URL_CANTAR_TRUCO, function(response) {
            if (response != USERNAME) {
                invalidarCartas()
                showInfo("Truco!!", 2500, color_j1)
                botonesInvisibles(['truco', 'envido', 'real_envido',
                                   'falta_envido']);
                $("#envido").unbind( "click" );
            } else {
                showInfo("Truco no disponible.", 2000, color_s)
            }
        });
    }
    obtenerTurnoCanto(cantarTruco);
}

function reTruco() {
    var cantarReTruco = function() {
        $.get(URL_CANTAR_RE_TRUCO, function(response) {
            if (response != USERNAME) {
                invalidarCartas()
                showInfo("Quiero Re Truco!!", 2500, color_j1)
                botonesInvisibles(['re_truco', 'quiero', 'no_quiero']);
            } else {
                showInfo("Re Truco no disponible.", 2000, color_s)
            }
        });
    }
    obtenerTurnoCanto(cantarReTruco);
}

function quieroValeCuatro(){
    var cantarQuieroValeCuatro = function() {
        $.get(URL_CANTAR_VALE_CUATRO, function(response) {
            if (response != USERNAME) {
                invalidarCartas()
                showInfo("Quiero Vale Cuatro!!", 2500, color_j1)
                botonesInvisibles(['quiero_valecuatro', 'quiero', 'no_quiero']);
            } else {
                showInfo("Quiero Vale cuatro no disponible.", 2000, color_s)
            }
        });
    }
    obtenerTurnoCanto(cantarQuieroValeCuatro);
}

function quieroTruco() {
    var cantarQuiero = function() {
        $.get(URL_CANTAR_QUIERO, function(response) {
            validarCartas();     
            showInfo("Quiero!", 2000, color_j1);
            botonesInvisibles(['quiero', 'no_quiero']);
        });
    }
    obtenerTurnoCanto(cantarQuiero);
}

function noQuieroTruco() {
    var cantarNoQuiero = function() {
        $.get(URL_CANTAR_NO_QUIERO, function(response) {
            invalidarCartas()
            clearInterval(interval2)
            showInfo("No Quiero!", 2000, color_j1);
            botonesInvisibles(['truco', 're_truco', 'quiero_valecuatro',
                               'envido', 'real_envido', 'falta_envido',
                               'quiero', 'no_quiero', 'ir_al_mazo']);
            setTimeout( function() {
                siguienteRonda();
                setTimeout( function() {
                    $(location).attr('href', URL_JUEGO);
                }, 2000);
            }, 2000);
        });
    }
    obtenerTurnoCanto(cantarNoQuiero);
}

function quieroEnvido() {
    var cantarQuiero = function() {
        $.get(URL_CANTAR_QUIERO, function(response) {
            showInfo("Quiero!", 2000, color_j1);
            botonesInvisibles(['envido', 'real_envido', 'falta_envido',
                               'quiero', 'no_quiero']);
            ganadorDeEnvido();
        });
    }
    obtenerTurnoCanto(cantarQuiero);
}

function noQuieroEnvido() {
    var cantarNoQuiero = function() {
        $.get(URL_CANTAR_NO_QUIERO, function(response) {
            showInfo("No Quiero!", 2000, color_j1);
            validarCartas();
            botonesInvisibles(['envido', 'real_envido', 'falta_envido',
                               'quiero', 'no_quiero']);
            botonesVisibles(['truco']);
            $("#truco").click(truco);
        });
    }
    obtenerTurnoCanto(cantarNoQuiero);
}

function ganadorDeEnvido() {
    $.get(URL_GANADOR_DE_ENVIDO, function(response) {
        if (response == USERNAME) {
            obtenerPuntosDeEnvido();
            botonesVisibles(['puntos_envido']);
            $("#puntos_envido").on("click", cantarPuntosEnvido);
        } else {
            botonesVisibles(['son_buenas']);
            $("#son_buenas").on("click", cantarSonBuenas);
        }
    });
}

function obtenerPuntosDeEnvido() {
    $.get(URL_OBTENER_PUNTOS_ENVIDO, function(response) {
        document.getElementById('puntos_envido').innerHTML = "Tengo " + String(response)
    });
}

function cantarPuntosEnvido() {
    var cantarPuntos = function() {
        $.get(URL_CANTAR_PUNTOS_DE_ENVIDO, function(response) {
            if (response != USERNAME) {
                text = document.getElementById('puntos_envido').innerHTML;
                showInfo(text+"!", 2000, color_j1);
                botonesInvisibles(['puntos_envido', 'son_buenas']);
                $("#puntos_envido").unbind( "click" );
            }
        });
    }
    obtenerTurnoCanto(cantarPuntos);
}

function cantarSonBuenas() {
    var sonBuenas = function() {
        $.get(URL_CANTAR_SON_BUENAS, function(response) {
            if (response != USERNAME) {
                showInfo("son Buenas!", 2000, color_j1);
                botonesInvisibles(['puntos_envido', 'son_buenas']);
                $("#puntos_envido").unbind( "click" );
                validarCartas();
            }
        });
    }
    obtenerTurnoCanto(sonBuenas); 
}

function irAlMazo() {
    var cantarIrAlMazo = function() {
        $.get(URL_IR_AL_MAZO, function(response) {
            invalidarCartas()
            clearInterval(interval2)
            showInfo("Me voy al Mazo!", 2000, color_j1);
            botonesInvisibles(['truco', 're_truco', 'quiero_valecuatro',
                               'envido', 'real_envido', 'falta_envido',
                               'quiero', 'no_quiero', 'ir_al_mazo']);
            setTimeout( function() {
                siguienteRonda();
                setTimeout( function() {
                    $(location).attr('href', URL_JUEGO);
                }, 2000);
            }, 2000);
        });
    }
    obtenerTurnoCanto(cantarIrAlMazo);
}

// muestra un cuadro informativo con el msj dado en el tiempo especificado
function showInfo(msg, time, color) {
    if(typeof(time)==='undefined') time = 2000;
    if(typeof(color)==='undefined') color = '#B43104';
    document.getElementById('table_info').style.display = 'block';
    document.getElementById('msg').style.color = color;
    $("#msg").html(msg)
    setTimeout(function() {
        document.getElementById('table_info').style.display = 'none';
    }, time);
}

function resetStyles1() {
    $("#carta1".concat(" img")).css({"top": "50px"});
    $("#carta1".concat(" img")).css({"border": "none"});
    $("#carta2".concat(" img")).css({"top": "50px"});
    $("#carta2".concat(" img")).css({"border": "none"});
    $("#carta3".concat(" img")).css({"top": "50px"});
    $("#carta3".concat(" img")).css({"border": "none"});
}

function resetStyles2() {
    $("#carta4".concat(" img")).css({"top": "0px"});
    $("#carta4".concat(" img")).css({"border": "none"});
    $("#carta5".concat(" img")).css({"top": "0px"});
    $("#carta5".concat(" img")).css({"border": "none"});
    $("#carta6".concat(" img")).css({"top": "0px"});
    $("#carta6".concat(" img")).css({"border": "none"});
}

function resetStyles3() {
    $("#carta7".concat(" img")).css({"top": "0px"});
    $("#carta8".concat(" img")).css({"border": "none"});
    $("#carta9".concat(" img")).css({"top": "0px"});
    $("#carta7".concat(" img")).css({"border": "none"});
    $("#carta8".concat(" img")).css({"top": "0px"});
    $("#carta9".concat(" img")).css({"border": "none"});
}

function resetStyles4() {
    $("#carta10".concat(" img")).css({"top": "0px"});
    $("#carta11".concat(" img")).css({"border": "none"});
    $("#carta12".concat(" img")).css({"top": "0px"});
    $("#carta10".concat(" img")).css({"border": "none"});
    $("#carta11".concat(" img")).css({"top": "0px"});
    $("#carta12".concat(" img")).css({"border": "none"});
}

function botonesVisibles(lista) {
    for (i=0; i<lista.length; i++) {
        document.getElementById(lista[i]).style.display = 'inline-table';
    }
}

function botonesInvisibles(lista) {
    for (i=0; i<lista.length; i++) {
        document.getElementById(lista[i]).style.display = 'none';
    }
}

        // if (CARTAS_REPARTIDAS) {
            // cartaDelOponente();
        // }
        // if (TRUCO && TURNO == USERNAME){
        //     invalidarCartas()
        //     document.getElementById('truco').style.display = 'none';
        //     document.getElementById('envido').style.display = 'none';
        //     document.getElementById('re_truco').style.display = 'inline-table';
        //     document.getElementById('quiero').style.display = 'inline-table';
        //     document.getElementById('no_quiero').style.display = 'inline-table';
        // } else if (TRUCO && TURNO != USERNAME) {
        //     document.getElementById('truco').style.display = 'none';
        //     document.getElementById('envido').style.display = 'none';
        // } else {
        //     document.getElementById('quiero').style.display = 'none';
        //     document.getElementById('no_quiero').style.display = 'none';
        // }