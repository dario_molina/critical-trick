 # -*- coding: utf-8 -*-

from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate
from django.contrib import messages
from django.template.response import TemplateResponse
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from critical_trick.forms import *
from critical_trick.models import *
import json
from django.views.decorators.csrf import csrf_exempt


def login(request):
    if request.method == "GET":
        form = LoginForm()
        context = {'form': form}
        return TemplateResponse(request, 'critical_trick/login.html', context)
    elif request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return redirect('critical_trick:menu')
        else:
            msg_error = "Usuario inválido o contraseña incorrecta." \
                        " Intente de nuevo"
            form = LoginForm()
            context = {'form': form, 'message': msg_error, 'error': True}
            return TemplateResponse(request, 'critical_trick/login.html',
                                    context)


def logout(request):
    auth_logout(request)
    message = "El usuario se a deslogueado exitosamente."
    messages.success(request, message)
    response = redirect('critical_trick:login')
    response.delete_cookie('user_location')
    return response


def register(request):
    if request.method == "GET":
        form = RegisterForm()
        context = {'form': form}
        return TemplateResponse(request, 'critical_trick/register.html',
                                context)
    elif request.method == "POST":
        try:
            form = RegisterForm(request.POST)
            if form.is_valid():
                form.save()
                message = ("El usuario ah sido exitosamente registrado!")
                messages.success(request, message)
                return redirect('critical_trick:login')
        except Exception as e:
            form = RegisterForm()
            context = {'form': form, 'message': e.message, 'error': True}
            return TemplateResponse(request, 'critical_trick/register.html',
                                    context)
    msg_error = form.errors.as_text
    form = RegisterForm()
    context = {'form': form, 'message': msg_error, 'error': True}
    return TemplateResponse(request, 'critical_trick/register.html', context)


@login_required
def menu(request):
    if request.method == "GET":
        request_dict = request.GET.dict()
        if not request_dict:
            if request.user.is_authenticated():
                partidas = Partida.objects.all()
                context = {'partidas': partidas}
                return TemplateResponse(request, 'critical_trick/menu.html',
                                        context)
            else:
                response = '<h1>Solo usuarios logeados pueden ingresar</h1>'
                return HttpResponse(response)
        else:
            if request_dict['estado'] == "buscando jugadores":
                partida = Partida.objects.get(id=request_dict['partida_id'])
                jugadores = Jugador.objects.filter(partida=partida)
                return HttpResponse(str(len(jugadores)))
            elif request_dict['estado'] == "actualizar mesas disponibles":
                num_partidas = len(Partida.objects.all())
                return HttpResponse(str(num_partidas))


def crearPartida(request):
    if request.method == "GET":
        form = PartidaForm()
        context = {'partidaForm': form}
        return TemplateResponse(request, 'critical_trick/crear_partida.html',
                                context)
    elif request.method == "POST":
        form = PartidaForm(request.POST)
        if form.is_valid():
            partida = form.save()
            j1 = Jugador()
            j1.crearPartida(request.user.username, partida.id)
            context = {'partida': partida}
            return TemplateResponse(request, 'critical_trick/espera.html',
                                    context)


def sumarseAPartida(request, match_id):
    try:
        partida = Partida.objects.get(id=match_id)
        jugadores = Jugador.objects.filter(partida=partida)
        if len(jugadores) < partida.maximo_de_jugadores - 1:
            j2 = Jugador()
            j2.sumarseAPartida(request.user.username, match_id)
            context = {'partida': partida}
            return TemplateResponse(request, 'critical_trick/espera.html',
                                    context)
        elif len(jugadores) == partida.maximo_de_jugadores - 1:
            j2 = Jugador()
            j2.sumarseAPartida(request.user.username, match_id)
            return redirect('critical_trick:juego', match_id=match_id)
        else:
            raise Exception("No hay lugar.\n La mesa está llena.")
    except Exception as msg_error:
        messages.success(request, msg_error)
        return redirect('critical_trick:menu')


def juego(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    jugadores = Jugador.objects.filter(partida=partida)
    ronda_actual = Ronda.objects.get(partida=partida,
                                     id_ronda=partida.numero_de_rondas)
    context = {'partida': partida, 'jugadores': jugadores,
               'player': jugador, 'ronda': ronda_actual}
    if partida.maximo_de_jugadores == 2:
        response = TemplateResponse(request, 'critical_trick/2Jugadores.html', context)
    elif partida.maximo_de_jugadores == 4:
        response = TemplateResponse(request, 'critical_trick/4Jugadores.html', context)
    elif partida.maximo_de_jugadores == 6:
        response = TemplateResponse(request, 'critical_trick/6Jugadores.html', context)
    return response

def nombresJugadores(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.nombresJugadores()
    return HttpResponse(json.dumps(response), content_type="application/json")

def repartirCartas(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.repartirCartas()
    return HttpResponse(response)

def cartasDelJugador(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.obtenerMisCartas()
    return HttpResponse(json.dumps(response), content_type="application/json")


def cartasDelOponente(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    request_dict = request.GET.dict()
    silla = request_dict['silla']
    response = jugador.obtenerCartasDelOponente(silla)
    return HttpResponse(json.dumps(response), content_type="application/json")


def obtenerTurno(request, match_id):
    partida = Partida.objects.get(id=match_id)
    ronda_actual = Ronda.objects.get(partida=partida,
                                     id_ronda=partida.numero_de_rondas)
    return HttpResponse(ronda_actual.turno)


def obtenerTurnoCanto(request, match_id):
    partida = Partida.objects.get(id=match_id)
    ronda_actual = Ronda.objects.get(partida=partida,
                                     id_ronda=partida.numero_de_rondas)
    return HttpResponse(ronda_actual.turno_canto)


def conocerJugadas(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.conocerJugadas()
    return HttpResponse(json.dumps(response), content_type="application/json")


def seleccionarCarta(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    request_dict = request.GET.dict()
    carta = request_dict['carta']
    response = jugador.elegirCarta(carta)
    ganadorDeMano(request, match_id) # corregir
    return HttpResponse(response)


def ganadorDeMano(request, match_id):
    partida = Partida.objects.get(id=match_id)
    ronda_actual = Ronda.objects.get(partida=partida,
                                     id_ronda=partida.numero_de_rondas)
    num_mano_actual = ronda_actual.numero_de_manos_jugadas + 1
    mano = Mano.objects.get(ronda=ronda_actual, id_mano=num_mano_actual)
    response = mano.ganadorDeMano()

def ganadorDeRonda(request, match_id):
    partida = Partida.objects.get(id=match_id)
    ronda_actual = Ronda.objects.get(partida=partida,
                                     id_ronda=partida.numero_de_rondas)
    response = ronda_actual.ganadorRonda()
    return HttpResponse(response)


def siguienteRonda(request, match_id):
    # request_dict = request.GET.dict()
    # ganador = request_dict['ganador']
    partida = Partida.objects.get(id=match_id)
    partida.siguienteRonda()
    return HttpResponse()


def cantarEnvido(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.envido()
    return HttpResponse(response)


def cantarRealEnvido(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.realEnvido()
    return HttpResponse(response)


def cantarFaltaEnvido(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.faltaEnvido()
    return HttpResponse(response)


def puntosDeEnvido(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    return HttpResponse(jugador.puntos_envido)


def cantarPuntosEnvido(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.cantarPuntosEnvido()
    return HttpResponse(response)


def cantarSonBuenas(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.sonBuenas()
    return HttpResponse(response)


def ganadorDeEnvido(request, match_id):
    partida = Partida.objects.get(id=match_id)
    ronda_actual = Ronda.objects.get(partida=partida,
                                     id_ronda=partida.numero_de_rondas)
    response = ronda_actual.ganadorDeEnvido()
    return HttpResponse(response)


def cantarTruco(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.truco()
    return HttpResponse(response)


def cantarQuiero(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.quiero()
    return HttpResponse(response)


def cantarNoQuiero(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.noQuiero()
    return HttpResponse(response)


def irAlMazo(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    jugador.irAlMazo()
    return HttpResponse()
    # response = redirect('critical_trick:siguiente_ronda', match_id=match_id)
    # response['Location'] += ('?ganador=' + str(ganador))


def cantarReTruco(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.reTruco()
    return HttpResponse(response)


def cantarQuieroValeCuatro(request, match_id):
    partida = Partida.objects.get(id=match_id)
    user = User.objects.get(username=request.user.username)
    jugador = Jugador.objects.get(user=user, partida=partida)
    response = jugador.quieroValeCuatro()
    return HttpResponse(response)

def lugarRepartidorDeCartas(request, match_id):
    partida = Partida.objects.get(id=match_id)
    response = partida.lugarRepartidorDeCartas()
    return HttpResponse(response)
