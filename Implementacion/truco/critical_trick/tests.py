 # -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth.models import User
from critical_trick.models import *
from django.core.urlresolvers import reverse
from django.test.client import Client
from random import randint
import json


class RegistroTest(TestCase):
    """
    Registro de Usuario.
    """
    def setUp(self):
        cantidad = User.objects.count()
        self.assertEqual(cantidad, 0)

    def test_create_user(self):
        response = self.client.post(reverse('critical_trick:register'), form1)
        self.assertEqual(User.objects.count(), 1)
        usuario = User.objects.get()
        self.assertEqual(usuario.username, 'ironman')
        self.assertEqual(usuario.email, 'ironman@marvel.com')
        self.assertTrue(usuario.is_authenticated())
        self.assertRedirects(response, reverse('critical_trick:login'))

    def test_unique_user(self):
        """
        Caso Excepcional: Registrar un usuario ya registrado anteriormente
                          con el mismo nombre.
        """
        client1 = Client()
        client2 = Client()
        client1.post(reverse('critical_trick:register'), form1)
        response = client2.post(reverse('critical_trick:register'), form1_mismo_nombre)
        self.assertTrue(response.context['error'])
        self.assertEqual(response.context['message'],
                         'Un usuario con ese nombre ya existe.')
        self.assertEqual(User.objects.count(), 1)

    def test_different_passwords(self):
        """
        Caso Excepcional: Los campos de contraseña
                          no son ingresados correctamente.
        """
        msj_error = 'Los dos campos de contraseña no coinciden.'
        response = self.client.post(reverse('critical_trick:register'), form_error)
        self.assertTrue(response.context['error'])
        self.assertEqual(response.context['message'], msj_error)
        self.assertEqual(User.objects.count(), 0)

    def test_status_valid(self):
        response = self.client.get(reverse('critical_trick:register'))
        self.assertEqual(response.status_code, 200)


class LoginTest(TestCase):
    """
    Iniciar Sesion.
    """
    def setUp(self):
        response = self.client.post(('critical_trick:register'), form4)

    def test_login_user(self):
        client = Client()
        response = client.post(reverse('critical_trick:login'), form_login4)
        self.assertEqual(response.status_code, 200)
        # self.assertRedirects(response,'/critical_trick/menu/')

    def test_login_error(self):
        """
        Escenario excepcional: Logueo de un usuario inexistente.
        """
        msg_error = "Usuario inválido o contraseña incorrecta." \
                    " Intente de nuevo"
        client = Client()
        response = client.post(reverse('critical_trick:login'), form_login_error)
        self.assertTemplateUsed(response, 'critical_trick/login.html')
        self.assertContains(response, msg_error, count=1)

    def test_login_empty(self):
        """
        Escenario excepcional: Logueo de un usuario inexistente.
        """
        client = Client()
        response = client.post(reverse('critical_trick:login'), form_login_empty)
        self.assertEqual(Partida.objects.count(), 0)


class LogountTest(TestCase):
    """
    Cierra Sesion.
    """
    def setUp(self):
        response = self.client.post(reverse('critical_trick:register'), form4)
        response = self.client.post(reverse('critical_trick:login'),
                                    form_login4)
        self.assertRedirects(response, '/critical_trick/menu/')
        print response

    def test_cerrarSesion(self):
        response = self.client.post(reverse('critical_trick:logout'))
        self.assertRedirects(response, reverse('critical_trick:login'))


class CrearPartidaTest(TestCase):
    """
    Crear Una nueva partida. Caso Exitoso
    """
    def setUp(self):
        self.client = Client()
        self.client.post(reverse('critical_trick:register'), form4)
        self.client.post(reverse('critical_trick:login'), form_login4)

    def test_create_partida(self):
        response = self.client.post(reverse('critical_trick:crear_partida'),
                                    form_partida30)

        partida = Partida.objects.get()
        jugador = Jugador.objects.get()
        self.assertEqual(Partida.objects.count(), 1)
        self.assertEqual(Jugador.objects.count(), 1)
        self.assertEqual(partida.nombre_de_partida, 'truco supremo')
        self.assertEqual(partida.maximo_de_jugadores, 2)
        self.assertEqual(partida.puntos_a_jugar, 30)
        self.assertEqual(jugador.user.username, 'cap')
        self.assertEqual(jugador.user.email, 'cap@marvel.com')
        self.assertEqual(jugador.equipo, 1)
        self.assertEqual(jugador.silla, 1)


class SumarseAPartidaTest(TestCase):
    """
    Unirse a una partida ya creada. Caso Exitoso
    """
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        self.client1.post(reverse('critical_trick:register'), form1)
        self.client2.post(reverse('critical_trick:register'), form2)
        self.client1.post(reverse('critical_trick:login'), form_login1)
        self.client2.post(reverse('critical_trick:login'), form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id

    def test_sumarse_partida(self):
        """
        Escenario exitoso:
        """
        response = self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                            args=[self.partida_id]))

        partida = Partida.objects.get(id=self.partida_id)
        ronda = Ronda.objects.get(partida=partida,
                                  id_ronda=partida.numero_de_rondas)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)

        self.assertEqual(Jugador.objects.count(), 2)
        self.assertEqual(partida.numero_jugadores, 2)
        self.assertEqual(partida.numero_de_rondas, 1)
        self.assertEqual(ronda.partida, partida)
        self.assertEqual(ronda.id_ronda, 1)
        self.assertEqual(ronda.puntos_generales, 1)
        self.assertEqual(ronda.numero_de_manos_jugadas, 0)
        self.assertEqual(ronda.jugador_mano, player2.user.username)
        self.assertEqual(ronda.turno, player2.user.username)
        self.assertEqual(ronda.turno_canto, player2.user.username)
        self.assertFalse(ronda.cartas_repartidas)
        self.assertEqual(player2.user.username, 'hulk')
        self.assertEqual(player2.user.email, 'hulk@marvel.com')
        self.assertEqual(player2.equipo, 2)
        self.assertEqual(player2.silla, 2)
        self.assertRedirects(response, reverse('critical_trick:juego',
                                               args=[self.partida_id]))

    def test_sumarse_partida_llena(self):
        """
        Caso Excepcional:
        """
        response = self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                            args=[self.partida_id]))
        client3 = Client()
        self.client1.post(reverse('critical_trick:register'), form3)
        self.client1.post(reverse('critical_trick:login'), form_login3)
        response = client3.get(reverse('critical_trick:sumarse_a_partida',
                                       args=[self.partida_id]))

        partida = Partida.objects.get(id=self.partida_id)
        self.assertNotEqual(Jugador.objects.count(), 3)
        self.assertNotEqual(partida.numero_jugadores, 3)
        self.assertEqual(Jugador.objects.count(), 2)
        self.assertEqual(partida.numero_jugadores, 2)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('critical_trick:menu'),
                             target_status_code=302)


class RepartirCartasTest(TestCase):
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        response = self.client1.post(reverse('critical_trick:register'), form1)
        response = self.client2.post(reverse('critical_trick:register'), form2)
        response = self.client1.post(reverse('critical_trick:login'),
                                     form_login1)
        response = self.client2.post(reverse('critical_trick:login'),
                                     form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id
        response = self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                            args=[self.partida_id]))

    def test_jugador1_reparte_cartas(self):
        response = self.client1.get(reverse('critical_trick:repartir_cartas',
                                            args=[self.partida_id]))
        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)
        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)

        cartas_player1 = Carta.objects.filter(jugador=player1,
                                              ronda=ronda_actual)
        cartas_player2 = Carta.objects.filter(jugador=player2,
                                              ronda=ronda_actual)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(ronda_actual.jugador_mano, player2.user.username)
        self.assertEqual(ronda_actual.turno, player2.user.username)
        self.assertTrue(ronda_actual.cartas_repartidas)
        self.assertEqual(len(cartas_player1), 3)
        self.assertEqual(len(cartas_player2), 3)


class ElegirCartaTest(TestCase):
    """
    El jugador elige una de las tres cartas para jugar.
    """
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        self.client1.post(reverse('critical_trick:register'), form1)
        self.client2.post(reverse('critical_trick:register'), form2)
        self.client1.post(reverse('critical_trick:login'), form_login1)
        self.client2.post(reverse('critical_trick:login'), form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id
        self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:repartir_cartas',
                                 args=[self.partida_id]))

        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)

        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        cartas_p1 = Carta.objects.filter(jugador=player1, ronda=ronda_actual)
        i = randint(0, 2)
        self.carta_p1 = str(cartas_p1[i].palo) + " " + str(cartas_p1[i].numero)

        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)
        cartas_p2 = Carta.objects.filter(jugador=player2, ronda=ronda_actual)
        i = randint(0, 2)
        self.carta_p2 = str(cartas_p2[i].palo) + " " + str(cartas_p2[i].numero)

    def test_jugador_elegir_carta(self):
        """
        Caso Exitoso: jugador mano elige una carta.
        """
        response = self.client2.get(reverse('critical_trick:seleccionar_carta',
                                            args=[self.partida_id]),
                                    {'carta': self.carta_p2})

        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)
        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)
        palo, numero = self.carta_p2.split(" ")
        carta = Carta.objects.get(jugador=player2, ronda=ronda_actual,
                                  palo=palo, numero=numero)

        self.assertTrue(carta.disponible_en_mesa)
        self.assertTrue(carta.ultima)
        self.assertEqual(ronda_actual.turno, player1.user.username)
        self.assertEqual(ronda_actual.turno_canto, player1.user.username)
        nuevo_msg = json.loads(ronda_actual.jugadas)[str(player1.silla)].pop()
        self.assertEqual(nuevo_msg["jugada"], "eleccion de carta")
        self.assertEqual(response.status_code, 200)


# class GanadorDeManoTest(TestCase):
#     """
#     Se llama a ganador de mano luego de que jugador 1 y 2 muestran
#     sus cartas.
#     """
#     def setUp(self):
#         self.client1 = Client()
#         self.client2 = Client()
#         self.client1.post(reverse('critical_trick:register'), form1)
#         self.client2.post(reverse('critical_trick:register'), form2)
#         self.client1.post(reverse('critical_trick:login'), form_login1)
#         self.client2.post(reverse('critical_trick:login'), form_login2)
#         response = self.client1.post(reverse('critical_trick:crear_partida'),
#                                      form_partida)
#         partida = response.context['partida']
#         self.partida_id = partida.id
#         self.client2.get(reverse('critical_trick:sumarse_a_partida',
#                                  args=[self.partida_id]))
#         self.client1.get(reverse('critical_trick:repartir_cartas',
#                                  args=[self.partida_id]))

#         partida = Partida.objects.get(id=self.partida_id)
#         ronda_actual = Ronda.objects.get(partida=partida,
#                                          id_ronda=partida.numero_de_rondas)

#         user1 = User.objects.get(username="ironman")
#         player1 = Jugador.objects.get(user=user1, partida=partida)
#         cartas_p1 = Carta.objects.filter(jugador=player1, ronda=ronda_actual)
#         i = randint(0, 2)
#         self.carta_p1 = str(cartas_p1[i].palo) + " " + str(cartas_p1[i].numero)

#         user2 = User.objects.get(username="hulk")
#         player2 = Jugador.objects.get(user=user2, partida=partida)
#         cartas_p2 = Carta.objects.filter(jugador=player2, ronda=ronda_actual)
#         i = randint(0, 2)
#         self.carta_p2 = str(cartas_p2[i].palo) + " " + str(cartas_p2[i].numero)

#     def test_ganador_de_mano_excepcional(self):
#         """
#         Caso Excepcional: jugador 2 elige una carta y llama a
#                           ganador de mano pero no hay ganador
#                           todavia.
#         """
#         self.client2.get(reverse('critical_trick:seleccionar_carta',
#                                  args=[self.partida_id]),
#                          {'carta': self.carta_p2})
#         response = self.client2.get(reverse('critical_trick:ganador_de_mano',
#                                             args=[self.partida_id]))

#         partida = Partida.objects.get(id=self.partida_id)
#         ronda_actual = Ronda.objects.get(partida=partida,
#                                          id_ronda=partida.numero_de_rondas)
#         num_mano_actual = ronda_actual.numero_de_manos_jugadas + 1
#         mano = Mano(ronda=ronda_actual, id_mano=num_mano_actual)

#         self.assertEqual(ronda_actual.numero_mano_ganador, 0)
#         self.assertFalse(mano.finalizada)
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(response.content, "falta una carta")

#     def test_consulta_ganador_de_mano_excepcional(self):
#         """
#         Caso Excepcional: jugador 2 elige una carta y jugador 1 llama a
#                           consultar ganador de mano pero no hay ganador
#                           todavia.
#         """
#         self.client2.get(reverse('critical_trick:seleccionar_carta',
#                                  args=[self.partida_id]),
#                          {'carta': self.carta_p2})
#         response = self.client1.get(reverse(
#                                     'critical_trick:consultar_ganador_de_mano',
#                                     args=[self.partida_id]))

#         partida = Partida.objects.get(id=self.partida_id)
#         ronda_actual = Ronda.objects.get(partida=partida,
#                                          id_ronda=partida.numero_de_rondas)
#         num_mano_actual = ronda_actual.numero_de_manos_jugadas + 1
#         mano = Mano(ronda=ronda_actual, id_mano=num_mano_actual)

#         self.assertEqual(ronda_actual.numero_mano_ganador, 0)
#         self.assertFalse(mano.finalizada)
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(response.content, "falta una carta")

#     def test_ganador_de_mano_exitoso(self):
#         """
#         Caso Excepcional: jugador 2 y jugador 1 eligen una carta y
#                           llama a ganador de mano el jugador 2.
#         """
#         self.client2.get(reverse('critical_trick:seleccionar_carta',
#                                  args=[self.partida_id]),
#                          {'carta': self.carta_p2})
#         self.client1.get(reverse('critical_trick:seleccionar_carta',
#                                  args=[self.partida_id]),
#                          {'carta': self.carta_p1})
#         response = self.client1.get(reverse('critical_trick:ganador_de_mano',
#                                             args=[self.partida_id]))

#         partida = Partida.objects.get(id=self.partida_id)
#         ronda = Ronda.objects.get(partida=partida,
#                                   id_ronda=partida.numero_de_rondas)
#         mano = Mano.objects.get(ronda=ronda, id_mano=1)

#         self.assertEqual(response.status_code, 200)
#         ganador = response.content
#         if ganador != "Empate":
#             self.assertEqual(ronda.turno, ganador)
#             self.assertEqual(ronda.turno_canto, ganador)
#         else:
#             self.assertEqual(ronda.turno, ronda.jugador_mano)
#             self.assertEqual(ronda.turno_canto, ronda.jugador_mano)
#         self.assertEqual(ronda.ganador_mano1, ganador)
#         self.assertEqual(ronda.numero_de_manos_jugadas, 1)
#         self.assertEqual(ronda.numero_mano_ganador, 1)
#         self.assertTrue(mano.finalizada)

#     def test_consulta_ganador_de_mano_exitoso(self):
#         """
#         Caso Excepcional: jugador 2 y jugador 1 eligen una carta y
#                           llama a ganador de mano el jugador 2.
#         """
#         self.client2.get(reverse('critical_trick:seleccionar_carta',
#                                  args=[self.partida_id]),
#                          {'carta': self.carta_p2})
#         self.client1.get(reverse('critical_trick:seleccionar_carta',
#                                  args=[self.partida_id]),
#                          {'carta': self.carta_p1})
#         self.client1.get(reverse('critical_trick:ganador_de_mano',
#                                  args=[self.partida_id]))
#         response = self.client2.get(reverse(
#                                     'critical_trick:consultar_ganador_de_mano',
#                                     args=[self.partida_id]))

#         partida = Partida.objects.get(id=self.partida_id)
#         ronda = Ronda.objects.get(partida=partida,
#                                   id_ronda=partida.numero_de_rondas)
#         mano = Mano.objects.get(ronda=ronda, id_mano=1)

#         self.assertEqual(response.content, ronda.ganador_mano1)
#         self.assertEqual(ronda.numero_mano_ganador, 0)
#         self.assertTrue(mano.finalizada)
#         self.assertEqual(response.status_code, 200)


class TrucoTest(TestCase):
    """
    El jugador canta truco dentro de una partida. Caso Exitoso
    """
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        self.client1.post(reverse('critical_trick:register'), form1)
        self.client2.post(reverse('critical_trick:register'), form2)
        self.client1.post(reverse('critical_trick:login'), form_login1)
        self.client2.post(reverse('critical_trick:login'), form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id
        self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:repartir_cartas',
                                 args=[self.partida_id]))

    def test_cantar_truco(self):
        self.client2.get(reverse('critical_trick:cantar_truco',
                                 args=[self.partida_id]))

        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)
        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)

        self.assertTrue(ronda_actual.truco)
        nuevo_msg = json.loads(ronda_actual.jugadas)[str(player1.silla)].pop()
        self.assertEqual(nuevo_msg["jugada"], "truco")
        self.assertEqual(ronda_actual.turno, player2.user.username)
        self.assertEqual(ronda_actual.turno_canto, player1.user.username)

class ReTrucoTest(TestCase):
    """
    El jugador canta  re truco dentro de una partida. Caso Exitoso
    """
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        self.client1.post(reverse('critical_trick:register'), form1)
        self.client2.post(reverse('critical_trick:register'), form2)
        self.client1.post(reverse('critical_trick:login'), form_login1)
        self.client2.post(reverse('critical_trick:login'), form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id
        self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:repartir_cartas',
                                 args=[self.partida_id]))

    def test_cantar_re_truco(self):
        self.client2.get(reverse('critical_trick:cantar_truco',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:cantar_re_truco',
                                 args=[self.partida_id]))

        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)
        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)

        self.assertTrue(ronda_actual.re_truco)
        nuevo_msg = json.loads(ronda_actual.jugadas)[str(player2.silla)].pop()
        self.assertEqual(nuevo_msg["jugada"], "re truco")
        self.assertEqual(ronda_actual.turno, player2.user.username)
        self.assertEqual(ronda_actual.turno_canto, player2.user.username)

class QuieroValeCuatroTest(TestCase):
    """
    El jugador canta quiero vale cuatro dentro de una partida.
    Caso Exitoso
    """
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        self.client1.post(reverse('critical_trick:register'), form1)
        self.client2.post(reverse('critical_trick:register'), form2)
        self.client1.post(reverse('critical_trick:login'), form_login1)
        self.client2.post(reverse('critical_trick:login'), form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id
        self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:repartir_cartas',
                                 args=[self.partida_id]))

    def test_cantar_quieroValeCuatro(self):
        self.client2.get(reverse('critical_trick:cantar_truco',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:cantar_re_truco',
                                 args=[self.partida_id]))
        self.client2.get(reverse('critical_trick:cantar_quiero_vale_cuatro',
                                 args=[self.partida_id]))

        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)
        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)
        self.assertTrue(ronda_actual.quiero_valecuatro)
        nuevo_msg = json.loads(ronda_actual.jugadas)[str(player1.silla)].pop()
        self.assertEqual(nuevo_msg["jugada"], "quiero vale cuatro")
        self.assertEqual(ronda_actual.turno, player2.user.username)
        self.assertEqual(ronda_actual.turno_canto, player1.user.username)



class QuieroTrucoTest(TestCase):
    """
    El jugador canta quiero despues de que hayan cantado truco.
    """
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        self.client1.post(reverse('critical_trick:register'), form1)
        self.client2.post(reverse('critical_trick:register'), form2)
        self.client1.post(reverse('critical_trick:login'), form_login1)
        self.client2.post(reverse('critical_trick:login'), form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id
        self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:repartir_cartas',
                                 args=[self.partida_id]))
        self.client2.get(reverse('critical_trick:cantar_truco',
                                 args=[self.partida_id]))

    def test_cantar_quiero_a_truco(self):
        self.client1.get(reverse('critical_trick:cantar_quiero',
                                 args=[self.partida_id]))

        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)
        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)

        self.assertEqual(ronda_actual.turno, player2.user.username)
        self.assertEqual(ronda_actual.turno_canto, player2.user.username)
        self.assertTrue(ronda_actual.quiero_truco, 1)
        nuevo_msg = json.loads(ronda_actual.jugadas)[str(player2.silla)].pop()
        self.assertEqual(nuevo_msg["jugada"], "quiero truco")
        self.assertEqual(ronda_actual.puntos_generales, 2)

class QuieroEnvidoTest(TestCase):
    """
    El jugador canta quiero despues de que hayan cantado truco.
    """
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        self.client1.post(reverse('critical_trick:register'), form1)
        self.client2.post(reverse('critical_trick:register'), form2)
        self.client1.post(reverse('critical_trick:login'), form_login1)
        self.client2.post(reverse('critical_trick:login'), form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id
        self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:repartir_cartas',
                                 args=[self.partida_id]))
        self.client2.get(reverse('critical_trick:cantar_envido',
                                 args=[self.partida_id]))

    def testCantarQuieroEnvido(self):
        self.client1.get(reverse('critical_trick:cantar_quiero',
                                 args=[self.partida_id]))
        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)
        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)
        self.assertEqual(ronda_actual.turno, player2.user.username)
        self.assertEqual(ronda_actual.turno_canto, player1.user.username)
        self.assertTrue(ronda_actual.envido, 1)
        self.assertEqual(ronda_actual.puntos_envido, 2)
        self.assertEqual(ronda_actual.puntos_generales, 1)
        nuevo_msg = json.loads(ronda_actual.jugadas)[str(player2.silla)].pop()
        self.assertEqual(nuevo_msg["jugada"], "quiero envido")

class QuieroEnvidoTrucoTest(TestCase):
    """
    El jugador cantar envido-quiero-truco-quiero.
    """
    def setUp(self):
        self.client1 = Client()
        self.client2 = Client()
        self.client1.post(reverse('critical_trick:register'), form1)
        self.client2.post(reverse('critical_trick:register'), form2)
        self.client1.post(reverse('critical_trick:login'), form_login1)
        self.client2.post(reverse('critical_trick:login'), form_login2)
        response = self.client1.post(reverse('critical_trick:crear_partida'),
                                     form_partida)
        partida = response.context['partida']
        self.partida_id = partida.id
        self.client2.get(reverse('critical_trick:sumarse_a_partida',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:repartir_cartas',
                                 args=[self.partida_id]))
        self.client2.get(reverse('critical_trick:cantar_envido',
                                 args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:cantar_quiero',
                                  args=[self.partida_id]))

    def testCantarQuiero(self):
        self.client1.get(reverse('critical_trick:cantar_truco',
                                  args=[self.partida_id]))
        self.client1.get(reverse('critical_trick:cantar_quiero',
                                 args=[self.partida_id]))
        partida = Partida.objects.get(id=self.partida_id)
        ronda_actual = Ronda.objects.get(partida=partida,
                                         id_ronda=partida.numero_de_rondas)
        user1 = User.objects.get(username="ironman")
        player1 = Jugador.objects.get(user=user1, partida=partida)
        user2 = User.objects.get(username="hulk")
        player2 = Jugador.objects.get(user=user2, partida=partida)
        self.assertEqual(ronda_actual.turno, player2.user.username)
        self.assertEqual(ronda_actual.turno_canto, player2.user.username)
        self.assertTrue(ronda_actual.envido, 1)
        self.assertEqual(ronda_actual.puntos_envido, 2)
        self.assertEqual(ronda_actual.puntos_generales, 2)