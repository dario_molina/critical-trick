from django.contrib import admin
from critical_trick.models import Jugador, Partida, Ronda, Carta, Mano
from django.contrib.auth.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'is_staff', 'is_active', 'last_login',
                    'date_joined')


class JugadorAdmin(admin.ModelAdmin):
    list_display = ('user', 'partida', 'equipo', 'silla', 'puntos_envido')


class PartidaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre_de_partida', 'maximo_de_jugadores',
                    'numero_jugadores', 'puntos_a_jugar', 'puntos_equipo1',
                    'puntos_equipo2', 'fecha_creacion', 'numero_de_rondas')


class RondaAdmin(admin.ModelAdmin):
    list_display = ('partida', 'id_ronda', 'puntos_generales', 'numero_de_manos_jugadas',
                    'equipo_ganador', 'jugador_mano', 'turno',
                    'cartas_repartidas', "ganador_mano1", "ganador_mano2",
                    'ganador_mano3', 'truco', 're_truco', 'envido',
                    'quiero_valecuatro', 'quiero_truco', 'real_envido',
                    'falta_envido')


class ManoAdmin(admin.ModelAdmin):
    list_display = ('ronda', 'id_mano', 'finalizada', 'carta_jugador1',
                    'carta_jugador2', 'carta_jugador3', 'carta_jugador4')


class CartaAdmin(admin.ModelAdmin):
    list_display = ('jugador', 'ronda', 'palo', 'numero', 'nivel',
                    'disponible_en_mesa')

admin.site.unregister(User)
admin.site.register(Carta, CartaAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Jugador, JugadorAdmin)
admin.site.register(Partida, PartidaAdmin)
admin.site.register(Ronda, RondaAdmin)
admin.site.register(Mano, ManoAdmin)
