from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^critical_trick/', include('critical_trick.urls', namespace='critical_trick')),
    url(r'^admin/', include(admin.site.urls)),
)
